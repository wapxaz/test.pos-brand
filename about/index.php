<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("TITLE", "О нас | Pos-brand +7 (499) 136-47-96");
$APPLICATION->SetPageProperty("description", "Рекламные конструкции из алюминиевых профилей и фурнитуры, широкий ассортимент товаров, оптовая поставка  | PosBrand. Гарантии качества. +7 (499) 136-47-96");
$APPLICATION->SetTitle("О нас");
?><p class="main_text">
	 Компания POS-BRAND — производственно-коммерческая фирма, работающая в направлении POS-индустрии.&nbsp;Нам удалось организовать полный цикл, начиная с производства профилей и заканчивая их переработкой в готовую продукцию. В нашей схеме отсутствуют какие-либо посредники, что позволяет гарантировать минимальные цены на всю линейку производимых нами товаров.
</p>
<p class="main_text">
	 Проект POS-BRAND был выделен как отдельное направление в конце 2015 года, однако производство успешно функционирует уже более пяти лет, что позволило нам выработать идеальную культуру производства. Качество всех производимых нами изделий соответствуют самым высоким европейским стандартам.
</p>
<p class="main_text">
	 Нами организованна складская программа по стандартным размерам всей производимой продукции, в связи с чем в большинстве случаев у нас отсутствует срок производства заказа, все есть в наличии. В случае индивидуальных и нестандартных заказов, постоянно большой склад алюминиевых профилей&nbsp;позволяет нам в кратчайшие сроки изготовить любой заказ.
</p>
<p class="main_text">
	 Линейка предлагаемых нами товаров достаточно широка, это и рамки из профилей NIELSEN, различных конфигураций и цветов, и КЛИК рамки с возможностью быстрой смены информации, визуальная навигация, информационные стойки, штендеры и многое другое. Также присутствует возможность изготовления индивидуальных заказов.
</p>
<p class="main_text">
	 Миссия компании POS-BRAND — эффективные и качественные решения в оформлении коммерческих площадей. Мы создаем действительно практичные, красивые и удобные рамки, световые панели, стенды, которые будут ежедневно радовать Вас и приносить практическую пользу в течении долгих лет.
</p>
<p class="main_text">
	 Мы всегда рады сотрудничеству с Вами!
</p>
<div id="container" style="height:600px;">
	<div id="tourDIV" style="height:600px;">
		<div id="panoDIV" style="height:600px;">
			<embed name="krpanoSWFObject" id="krpanoSWFObject" width="100%" height="100%" style="outline:none;" type="application/x-shockwave-flash" src="Altirodata/Altiro.swf" wmode="window" bgcolor="#000000" allowfullscreen="true" allowscriptaccess="always" flashvars="browser.useragent=Mozilla%2F5.0%20(Windows%20NT%2010.0%3B%20WOW64)%20AppleWebKit%2F537.36%20(KHTML%2C%20like%20Gecko)%20Chrome%2F53.0.2785.143%20Safari%2F537.36&amp;browser.location=http%3A%2F%2Fhtml.pos-brand.ru%2Fabout.php&amp;xml=Altirodata%2FAltiro.xml">
		</div>
		 <script type="text/javascript" src="Altirodata/Altiro.js"></script> <script type="text/javascript">

          embedpano({

          swf:"Altirodata/Altiro.swf"

          ,target:"panoDIV"
          ,passQueryParameters:true


          });

        </script>
	</div>
</div>
<br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>