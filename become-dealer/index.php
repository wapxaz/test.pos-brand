<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("TITLE", "Как стать дилером | Pos-brand +7 (499) 136-47-96");
$APPLICATION->SetPageProperty("description", "Рекламные конструкции из алюминиевых профилей и фурнитуры, компания развивает дилерскую сеть в регионах России  | PosBrand. Гарантии качества. +7 (499) 136-47-96");
$APPLICATION->SetPageProperty("show-form", "Y");
$APPLICATION->SetTitle("Как стать дилером?");
?><p class="main_text">
	Для удобства региональных клиентов компания Pos-Brand развивает дилерскую сеть в регионах России. Мы предоставляем специальные условия для наших региональных представителей:
</p>
<ul class="main_ul">
	<li>специальные цены;</li>
	<li>маркетинговую поддержку;</li>
	<li>обучение и консультации торгового персонала.</li>
</ul>
<p class="main_text">
	Если Вы хотите стать нашим дилером, отправьте нам сообщение!
</p><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>