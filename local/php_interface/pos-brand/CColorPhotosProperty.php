<?
use Bitrix\Main\Localization\Loc;

class CColorPhotosProperty
{
	protected static $directoryMap = array();

	/**
	 * Returns property type description.
	 *
	 * @return array
	 */
	public static function GetUserTypeDescription()
	{
		return array(
			'PROPERTY_TYPE' => 'S',
			"USER_TYPE" => "ColorPhotos",
			"DESCRIPTION" => "Фото для цвета",
			'GetSettingsHTML' => array(__CLASS__, 'GetSettingsHTML'),
			'GetPropertyFieldHtml' => array(__CLASS__, 'GetPropertyFieldHtml'),
			"ConvertToDB" => array(__CLASS__,"ConvertToDB"),
			"ConvertFromDB" => array(__CLASS__,"ConvertFromDB"),
			'PrepareSettings' => array(__CLASS__, 'PrepareSettings'),
		);
	}

	/**
	 * Prepare settings for property.
	 *
	 * @param array $arProperty				Property description.
	 * @return array
	 */
	public static function PrepareSettings($arProperty)
	{
		$size = 1;
		$width = 0;
		$group = "N";
		$ID = '';

		if (!empty($arProperty["USER_TYPE_SETTINGS"]) && is_array($arProperty["USER_TYPE_SETTINGS"]))
		{
			if (isset($arProperty["USER_TYPE_SETTINGS"]["size"]))
			{
				$size = (int)$arProperty["USER_TYPE_SETTINGS"]["size"];
				if ($size <= 0)
					$size = 1;
			}

			if (isset($arProperty["USER_TYPE_SETTINGS"]["width"]))
			{
				$width = (int)$arProperty["USER_TYPE_SETTINGS"]["width"];
				if ($width < 0)
					$width = 0;
			}

			if (isset($arProperty["USER_TYPE_SETTINGS"]["group"]) && $arProperty["USER_TYPE_SETTINGS"]["group"] === "Y")
				$group = "Y";

			if (isset($arProperty["USER_TYPE_SETTINGS"]["COLOR_PROP_ID"]))
				$COLOR_PROP_ID = (int)$arProperty["USER_TYPE_SETTINGS"]['COLOR_PROP_ID'];
		}
		return array(
			"size" =>  $size,
			"width" => $width,
			"group" => $group,
			"COLOR_PROP_ID" => $COLOR_PROP_ID,
		);
	}

	/**
	 * Returns html for show in edit property page.
	 *
	 * @param array $arProperty				Property description.
	 * @param array $strHTMLControlName		Control description.
	 * @param array $arPropertyFields		Property fields for edit form.
	 * @return string
	 */
	public static function GetSettingsHTML($arProperty, $strHTMLControlName, &$arPropertyFields)
	{
		$settings = self::PrepareSettings($arProperty);

		$arPropertyFields = array(
			"HIDE" => array("ROW_COUNT", "COL_COUNT", "MULTIPLE_CNT"),
		);

		$iblockID = 0;
		if (isset($arProperty['IBLOCK_ID']))
			$iblockID = (int)$arProperty['IBLOCK_ID'];

		$cellOption = '';
		$rsProperties = CIBlockProperty::GetList(Array("sort"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>$iblockID));
		while ($arProp = $rsProperties->GetNext())
		{
			$selected = ($settings["COLOR_PROP_ID"] == $arProp['ID']) ? ' selected' : '';
			$cellOption .= '<option '.$selected.' value="'.htmlspecialcharsbx($arProp["ID"]).'">'.htmlspecialcharsex($arProp["NAME"].' ('.$arProp["ID"].')').'</option>';
		}

		return '
			<tr>
				<td>Выберите свойство:</td>
				<td>
					<input type="hidden" name="'.$strHTMLControlName["NAME"].'[COLOR_PROP_ID]" disabled id="property_id_hidden">
					<select name="'.$strHTMLControlName["NAME"].'[COLOR_PROP_ID]" id="property_id">
						'.$cellOption.'
					</select>
				</td>
			</tr>';
	}

	/**
	 * Return html for edit single value.
	 *
	 * @param array $arProperty				Property description.
	 * @param array $value					Current value.
	 * @param array $strHTMLControlName		Control description.
	 * @return string
	 */
	public static function GetPropertyFieldHtml($arProperty, $arInputValue, $strHTMLControlName)
	{
		$settings = self::PrepareSettings($arProperty);
   		CModule::IncludeModule('fileman');
		$size = ($settings["size"] > 1 ? ' size="'.$settings["size"].'"' : '');
		$width = ($settings["width"] > 0 ? ' style="width:'.$settings["width"].'px"' : '');

		$iblockID = 0;
		if (isset($arProperty['IBLOCK_ID']))
			$iblockID = (int)$arProperty['IBLOCK_ID'];
		if(intval($_REQUEST['ID'])) {
			$res = CIBlockElement::GetProperty($iblockID, intval($_REQUEST['ID']), "sort", "asc", array("ID" => $settings['COLOR_PROP_ID']));
			while ($ob = $res->GetNext())
			{
				$VALUES[] = $ob;
			}
		}
		$html = '<div>';
		foreach ($VALUES as $arVal) {
			$inputName = array();
			foreach ($arInputValue as $intPropertyValueID => $arOneValue)
			{
				$key = $strHTMLControlName["VALUE"];
				foreach ($arOneValue[ $arVal['PROPERTY_VALUE_ID'] ] as $ind => $value) {
					$inputName[$key."[".$arVal['PROPERTY_VALUE_ID']."][".$ind."]"] = $value;
				}
			}

			ob_start();
			if (class_exists('\Bitrix\Main\UI\FileInput', true))
			{
				echo \Bitrix\Main\UI\FileInput::createInstance((
					array(
						"name" => $strHTMLControlName["VALUE"]."[n#IND#][".$arVal['PROPERTY_VALUE_ID']."]",
						"id" => $strHTMLControlName["VALUE"]."[n#IND#][".$arVal['PROPERTY_VALUE_ID']."]_".mt_rand(1, 1000000),
						"description" => $settings["WITH_DESCRIPTION"]=="Y",
						"allowUpload" => "F",
						"allowUploadExt" => $settings["FILE_TYPE"]
					) + ($historyId > 0 ? array(
						"delete" => false,
						"edit" => false
					) : array(
						"upload" => true,
						"medialib" => true,
						"fileDialog" => true,
						"cloud" => true
					))
				))->show($inputName);
			}
			else if($historyId > 0)
				echo CFileInput::ShowMultiple($inputName, $strHTMLControlName["VALUE"]."[n#IND#][".$arVal['PROPERTY_VALUE_ID']."]", array(
					"IMAGE" => "Y",
					"PATH" => "Y",
					"FILE_SIZE" => "Y",
					"DIMENSIONS" => "Y",
					"IMAGE_POPUP" => "Y",
					"MAX_SIZE" => $maxSize,
					), false);
			else
				echo CFileInput::ShowMultiple($inputName, $strHTMLControlName["VALUE"]."[n#IND#][".$arVal['PROPERTY_VALUE_ID']."]", array(
						"IMAGE" => "Y",
						"PATH" => "Y",
						"FILE_SIZE" => "Y",
						"DIMENSIONS" => "Y",
						"IMAGE_POPUP" => "Y",
						"MAX_SIZE" => $maxSize,
					), false, array(
						'upload' => true,
						'medialib' => true,
						'file_dialog' => true,
						'cloud' => true,
						'del' => true,
						'description' => $settings["WITH_DESCRIPTION"]=="Y",
					));
			$new = ob_get_clean();
			$html .= '<b>'.$arVal['DESCRIPTION'].'</b>:'.$new;
		}
		$html .= '</div>';
		return  $html;
	}
	static function ConvertToDB($arProperty, $value)
	{
		$result = array();
		$return = array();

		$arPhotoValues = array_shift($_REQUEST['PROP'][ $arProperty['ID'] ]);
		$arPhotoValuesDel = array_shift($_REQUEST['PROP_del'][ $arProperty['ID'] ]);
    	foreach ($arPhotoValues['VALUE'] as $ind => $arValues) {
    		if(preg_match('/^n(\d*)/', $ind)) {
	    		foreach ($arValues as $PROPERTY_VALUE_ID => $arFile) {
	    			if(!is_array($arFile) && strlen($arFile)) {
	    				$arFile = CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"].$arFile);
	    			} else {
						$arFile["tmp_name"] = $_SERVER["DOCUMENT_ROOT"].$arFile["tmp_name"];
	    			}

					$arFile["MODULE_ID"] = "iblock";
					$arFile["del"] = $_REQUEST["PROP[".$arProperty['ID']."][n0][VALUE][".$ind."][".$PROPERTY_VALUE_ID."][del]"];
	    			$nFile = CFile::SaveFile($arFile, 'iblock');
	    			$result['VALUE'][$PROPERTY_VALUE_ID][] = $nFile;
	    		}
    		} elseif(intval($ind)) {
    			foreach ($arValues as $nFile) {
    				$result['VALUE'][$ind][] = $nFile;
    			}
    		}
    	};
    	foreach ($arPhotoValuesDel['VALUE'] as $ind => $arValues) {
    		if(intval($ind)) {
    			foreach ($arValues as $nFileInd => $sDel) {
    				if($sDel == 'Y') {
						CFile::Delete($result['VALUE'][$ind][$nFileInd]);
						unset($result['VALUE'][$ind][$nFileInd]);
    				}
    			}
    		}
    	}/*
		echo '<pre>';
		var_dump($result['VALUE']);
		echo '</pre>';
		die();*/
		if(count($result["VALUE"])) {
			$return["VALUE"] = serialize($result["VALUE"]);
		} else {
			$return["VALUE"] = false;
		}
		$return["DESCRIPTION"] = '';
		
		return $return;
	}

	function ConvertFromDB($arProperty, $value)
	{
		$return = array();
		if (strLen(trim($value["VALUE"])) > 0) {
			$return["VALUE"] = unserialize($value["VALUE"]);
		} elseif(is_array($value["VALUE"])) {
			$return["VALUE"] = $value["VALUE"];
		}
		
		return $return;
	}
}