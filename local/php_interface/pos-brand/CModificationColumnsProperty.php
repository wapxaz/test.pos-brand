<?
use Bitrix\Main\Localization\Loc;

class CModificationColumnsProperty
{
	protected static $directoryMap = array();

	/**
	 * Returns property type description.
	 *
	 * @return array
	 */
	public static function GetUserTypeDescription()
	{
		return array(
			'PROPERTY_TYPE' => 'S',
			"USER_TYPE" => "ModificationColumns",
			"DESCRIPTION" => "Модификации",
			'GetSettingsHTML' => array(__CLASS__, 'GetSettingsHTML'),
			'GetPropertyFieldHtml' => array(__CLASS__, 'GetPropertyFieldHtml'),
			"ConvertToDB" => array(__CLASS__,"ConvertToDB"),
			"ConvertFromDB" => array(__CLASS__,"ConvertFromDB"),
			'PrepareSettings' => array(__CLASS__, 'PrepareSettings'),
		);
	}

	/**
	 * Prepare settings for property.
	 *
	 * @param array $arProperty				Property description.
	 * @return array
	 */
	public static function PrepareSettings($arProperty)
	{
		$size = 1;
		$width = 0;
		$group = "N";
		$ID = '';

		if (!empty($arProperty["USER_TYPE_SETTINGS"]) && is_array($arProperty["USER_TYPE_SETTINGS"]))
		{
			if (isset($arProperty["USER_TYPE_SETTINGS"]["size"]))
			{
				$size = (int)$arProperty["USER_TYPE_SETTINGS"]["size"];
				if ($size <= 0)
					$size = 1;
			}

			if (isset($arProperty["USER_TYPE_SETTINGS"]["width"]))
			{
				$width = (int)$arProperty["USER_TYPE_SETTINGS"]["width"];
				if ($width < 0)
					$width = 0;
			}

			if (isset($arProperty["USER_TYPE_SETTINGS"]["group"]) && $arProperty["USER_TYPE_SETTINGS"]["group"] === "Y")
				$group = "Y";

			if (isset($arProperty["USER_TYPE_SETTINGS"]["COLUMNS_PROP_ID"]))
				$COLUMNS_PROP_ID = (int)$arProperty["USER_TYPE_SETTINGS"]['COLUMNS_PROP_ID'];
		}
		return array(
			"size" =>  $size,
			"width" => $width,
			"group" => $group,
			"COLUMNS_PROP_ID" => $COLUMNS_PROP_ID,
		);
	}

	/**
	 * Returns html for show in edit property page.
	 *
	 * @param array $arProperty				Property description.
	 * @param array $strHTMLControlName		Control description.
	 * @param array $arPropertyFields		Property fields for edit form.
	 * @return string
	 */
	public static function GetSettingsHTML($arProperty, $strHTMLControlName, &$arPropertyFields)
	{
		$settings = self::PrepareSettings($arProperty);

		$arPropertyFields = array(
			"HIDE" => array("ROW_COUNT", "COL_COUNT", "MULTIPLE_CNT"),
		);

		$iblockID = 0;
		if (isset($arProperty['IBLOCK_ID']))
			$iblockID = (int)$arProperty['IBLOCK_ID'];

		$cellOption = '';
		$rsProperties = CIBlockProperty::GetList(Array("sort"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>$iblockID));
		while ($arProp = $rsProperties->GetNext())
		{
			$selected = ($settings["COLUMNS_PROP_ID"] == $arProp['ID']) ? ' selected' : '';
			$cellOption .= '<option '.$selected.' value="'.htmlspecialcharsbx($arProp["ID"]).'">'.htmlspecialcharsex($arProp["NAME"].' ('.$arProp["ID"].')').'</option>';
		}

		return '
			<tr>
				<td>Выберите свойство:</td>
				<td>
					<input type="hidden" name="'.$strHTMLControlName["NAME"].'[COLUMNS_PROP_ID]" disabled id="property_id_hidden">
					<select name="'.$strHTMLControlName["NAME"].'[COLUMNS_PROP_ID]" id="property_id">
						'.$cellOption.'
					</select>
				</td>
			</tr>';
	}

	/**
	 * Return html for edit single value.
	 *
	 * @param array $arProperty				Property description.
	 * @param array $value					Current value.
	 * @param array $strHTMLControlName		Control description.
	 * @return string
	 */
	public static function GetPropertyFieldHtml($arProperty, $value, $strHTMLControlName)
	{
		$settings = self::PrepareSettings($arProperty);
		$size = ($settings["size"] > 1 ? ' size="'.$settings["size"].'"' : '');
		$width = ($settings["width"] > 0 ? ' style="width:'.$settings["width"].'px"' : '');

		$iblockID = 0;
		if (isset($arProperty['IBLOCK_ID']))
			$iblockID = (int)$arProperty['IBLOCK_ID'];
		if(intval($_REQUEST['ID'])) {
			$res = CIBlockElement::GetProperty($iblockID, intval($_REQUEST['ID']), "sort", "asc", array("ID" => $settings['COLUMNS_PROP_ID']));
			while ($ob = $res->GetNext())
			{
				$VALUES[] = $ob;
			}
		}

		$html = '<table><tr>';
		foreach ($VALUES as $arVal) {
			$html .= '<td><b>'.$arVal['VALUE'].'</b>:<br/><input type="text" name="'.$strHTMLControlName["VALUE"].'['.$arVal['PROPERTY_VALUE_ID'].']"'.$size.$width.' value="'.$value['VALUE'][$arVal['PROPERTY_VALUE_ID']].'"></td><td>&nbsp;</td>';
		}
		$html .= '</tr></table><br/>';
		return  $html;
	}

	function ConvertToDB($arProperty, $value)
	{
		$result = array();
		$return = array();
		
		$result['VALUE'] = $value['VALUE'];

		if(count($result["VALUE"]) && $result['VALUE'][ array_keys($result['VALUE'])[0] ]) {
			$return["VALUE"] = serialize($result["VALUE"]);
		} else {
			$return["VALUE"] = false;
		}
		
		return $return;
	}

	function ConvertFromDB($arProperty, $value)
	{
		$return = array();
		if (strLen(trim($value["VALUE"])) > 0)
			$return["VALUE"] = unserialize($value["VALUE"]);
		
		return $return;
	}
}