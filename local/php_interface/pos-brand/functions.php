<?
function imagecreatefromfile( $filename ) {
    if (!file_exists($filename)) {
        throw new InvalidArgumentException('File "'.$filename.'" not found.');
    }
    switch ( strtolower( pathinfo( $filename, PATHINFO_EXTENSION ))) {
        case 'jpeg':
        case 'jpg':
            return imagecreatefromjpeg($filename);
        break;

        case 'png':
            return imagecreatefrompng($filename);
        break;

        case 'gif':
            return imagecreatefromgif($filename);
        break;

        default:
            throw new InvalidArgumentException('File "'.$filename.'" is not valid jpg, png or gif image.');
        break;
    }
}
function imagecopyrhomb(&$imgDst, $imgSrc, $hIgnoreColor) {
	$width = imagesx($imgSrc);
	$height = imagesy($imgSrc);
	for($x = 0; $x < $width; $x++) {
        $bNextIgnore = false;
        for($y = 0; $y < $height / 2; $y++) {
            $rgb = imagecolorat($imgSrc, $x, $y);
            if ( $hIgnoreColor == $rgb ) {
                if($bNextIgnore) imagesetpixel($imgDst, $x, $y, $rgb);
            } else {
                imagesetpixel($imgDst, $x, $y, $rgb);
                $bNextIgnore = true;
            }
        }
        $bNextIgnore = false;
		for($y = $height - 1; $y >= $height / 2; $y--) {
			$rgb = imagecolorat($imgSrc, $x, $y);
			if ( $hIgnoreColor == $rgb ) {
                if($bNextIgnore) imagesetpixel($imgDst, $x, $y, $rgb);
			} else {
				imagesetpixel($imgDst, $x, $y, $rgb);
                $bNextIgnore = true;
			}
		}
	}
}
function prepareRhombPic($strFileName) {
    $strAbsFileName = $_SERVER['DOCUMENT_ROOT'].$strFileName;
    $strAbsNewFileName = str_replace(pathinfo(($strAbsFileName), PATHINFO_EXTENSION), 'png', $strAbsFileName);
    $strFlagName = str_replace(pathinfo(($strAbsFileName), PATHINFO_EXTENSION), 'flag', $strAbsFileName);
    if(!file_exists($strFlagName)) {
		$hImg = imagecreatefromfile($strAbsFileName);
		list($nWidth, $nHeight) = getimagesize($strAbsFileName);
        $nSize = imagesx($hImg);
        $nSize2 = $nSize / 2;
		$hTmpImg = imagecreatetruecolor($nSize, $nSize);
		imagesavealpha($hTmpImg, true);
		$hTransparentColor = imagecolorallocatealpha($hTmpImg, 255, 255, 255, 127);
		$hWhite = imagecolorallocate($hTmpImg, 255, 255, 255);
		$hBlack = imagecolorallocate($hTmpImg, 0, 0, 0);

		imagecopyresampled($hTmpImg, $hImg, 0, 0, 0, 0, $nSize, $nSize, $nWidth, $nHeight);
		//LU
		imagefilledpolygon($hTmpImg, array(0, 0, 0, $nSize2, $nSize2, 0), 3, $hWhite);
		//RU
		imagefilledpolygon($hTmpImg, array($nSize, 0, $nSize, $nSize2, $nSize2, 0), 3, $hWhite);
		//LB
		imagefilledpolygon($hTmpImg, array(0, $nSize, $nSize2, $nSize, 0, $nSize2), 3, $hWhite);
		//RB
		imagefilledpolygon($hTmpImg, array($nSize, $nSize, $nSize2, $nSize, $nSize, $nSize2), 3, $hWhite);

		$hResImg = imagecreatetruecolor($nSize, $nSize);

		imagecolortransparent($hResImg, $hBlack);
		imagecopyrhomb($hResImg, $hTmpImg, $hWhite);

        imagepng($hResImg, $strAbsNewFileName);
        fclose(fopen($strFlagName, 'w+'));
    }
    return str_replace($_SERVER['DOCUMENT_ROOT'], '', $strAbsNewFileName);
}
?>