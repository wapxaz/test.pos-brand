<?
namespace PosBrand;

class CTemplate {
	function ShowPreContent() {
		ob_start();
		global $APPLICATION;
		$strPageType = $APPLICATION->GetProperty('page-type');
		if(strpos($strPageType, '2col') !== false) {
			?>
				<div class="container">
					<div class="row">
						<div class="sidebar col-md-3 col-sm-4">
							<?=self::ShowLeftColumn()?>
						</div>
						<article class="main col-md-9 col-sm-8">
			<?
		} elseif(strpos($strPageType, 'main-page') !== false) {
			?>
				<article class="main">
			<?
		} else {
			?>
				<article class="main">
					<div class="container">
			<?
		}
		if(strpos($strPageType, 'main-page') === false) {
			$APPLICATION->IncludeComponent(
				"pos-brand:breadcrumb.immediately",
				"",
				Array(
					"PATH" => "",
					"SITE_ID" => "s1",
					"START_FROM" => "0"
				)
			);
			if($APPLICATION->GetProperty('show-h1', 'Y') == 'Y') {
				echo '<h1 class="main_title">'.$APPLICATION->GetTitle().'</h1>';
			}
			if(strpos($strPageType, 'article') !== false) {
				echo '<div class="main_text-block">';
			}
		}
		if(strpos($strPageType, 'contacts') !== false) {
			?>
				<section class="contacts">
					<div class="contacts_block">
						<div class="contacts_block_item">
							<div class="contacts_block_item_title">АДРЕС</div>
							<div class="contacts_block_item_info"><?=\CNLSMainSettings::GetSiteSetting('pos_contacts_address')?></div>
						</div>
						<div class="contacts_block_item">
							<div class="contacts_block_item_title">ГРАФИК РАБОТЫ</div>
							<div class="contacts_block_item_info"><?=\CNLSMainSettings::GetSiteSetting('pos_contacts_worktime')?></div>
						</div>
						<div class="contacts_block_item">
							<div class="contacts_block_item_title">ТЕЛЕФОНЫ</div>
							<div class="contacts_block_item_info"><?=\CNLSMainSettings::GetSiteSetting('pos_contacts_phone')?></div>
						</div>
						<div class="contacts_block_item">
							<div class="contacts_block_item_title">ПОЧТА</div>
							<div class="contacts_block_item_info"><a href="mailto:<?=\CNLSMainSettings::GetSiteSetting('pos_contacts_email')?>"><?=\CNLSMainSettings::GetSiteSetting('pos_contacts_email')?></a></div>
						</div>
					</div>
					<div class="contacts_block_text">
			<?
		}
		return ob_get_clean();
	}
	function ShowPostContent() {
		ob_start();
		global $APPLICATION;
		$strPageType = $APPLICATION->GetProperty('page-type');
		if($APPLICATION->GetProperty('show-form', 'N') == 'Y') {
			$APPLICATION->IncludeComponent(
				"bitrix:form.result.new", 
				"become-dealer-form", 
				array(
					"CACHE_TIME" => "3600",
					"CACHE_TYPE" => "A",
					"CHAIN_ITEM_LINK" => "",
					"CHAIN_ITEM_TEXT" => "",
					"EDIT_URL" => "",
					"IGNORE_CUSTOM_TEMPLATE" => "Y",
					"LIST_URL" => "",
					"SEF_FOLDER" => "/become-dealer/",
					"SEF_MODE" => "Y",
					"SUCCESS_URL" => "",
					"USE_EXTENDED_ERRORS" => "N",
					"WEB_FORM_ID" => "1",
					"COMPONENT_TEMPLATE" => "become-dealer-form"
				),
				false
			);
		}
		if(strpos($strPageType, 'main-page') === false) {
			if(strpos($strPageType, 'article') !== false) {
				echo '</div>';
			}
		}
		if(strpos($strPageType, 'contacts') !== false) {
			?>
				</div>
				<div class="contacts_map">
					<script type="text/javascript" charset="utf-8" src="<?=\CNLSMainSettings::GetSiteSetting('pos_contacts_maplink')?>"></script>
				</div>
			</section>
			<?
		}
		if(strpos($strPageType, '2col') !== false) {
			?>
						</article>
					</div>
				</div>
			<?
		} elseif(strpos($strPageType, 'main-page') !== false) {
			?>
				</article>
			<?
		} else {
			?>
					</div>
				</article>
			<?
		}
		return ob_get_clean();
	}
	function ShowSlider() {
		ob_start();
		global $APPLICATION;
		$strPageType = $APPLICATION->GetProperty('page-type');
		if(strpos($strPageType, 'main-page') !== false) {
			$APPLICATION->IncludeComponent(
				"bitrix:news.list",
				"main-banner",
				Array(
					"ACTIVE_DATE_FORMAT" => "d.m.Y",
					"ADD_SECTIONS_CHAIN" => "N",
					"AJAX_MODE" => "N",
					"AJAX_OPTION_ADDITIONAL" => "",
					"AJAX_OPTION_HISTORY" => "N",
					"AJAX_OPTION_JUMP" => "N",
					"AJAX_OPTION_STYLE" => "Y",
					"CACHE_FILTER" => "N",
					"CACHE_GROUPS" => "Y",
					"CACHE_TIME" => "36000000",
					"CACHE_TYPE" => "A",
					"CHECK_DATES" => "Y",
					"DETAIL_URL" => "",
					"DISPLAY_BOTTOM_PAGER" => "N",
					"DISPLAY_DATE" => "Y",
					"DISPLAY_NAME" => "Y",
					"DISPLAY_PICTURE" => "Y",
					"DISPLAY_PREVIEW_TEXT" => "Y",
					"DISPLAY_TOP_PAGER" => "N",
					"FIELD_CODE" => array("", ""),
					"FILTER_NAME" => "",
					"HIDE_LINK_WHEN_NO_DETAIL" => "N",
					"IBLOCK_ID" => "1",
					"IBLOCK_TYPE" => "other",
					"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
					"INCLUDE_SUBSECTIONS" => "Y",
					"MESSAGE_404" => "",
					"NEWS_COUNT" => "20",
					"PAGER_BASE_LINK_ENABLE" => "N",
					"PAGER_DESC_NUMBERING" => "N",
					"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
					"PAGER_SHOW_ALL" => "N",
					"PAGER_SHOW_ALWAYS" => "N",
					"PAGER_TEMPLATE" => ".default",
					"PAGER_TITLE" => "Новости",
					"PARENT_SECTION" => "",
					"PARENT_SECTION_CODE" => "",
					"PREVIEW_TRUNCATE_LEN" => "",
					"PROPERTY_CODE" => array("LINK", ""),
					"SET_BROWSER_TITLE" => "N",
					"SET_LAST_MODIFIED" => "N",
					"SET_META_DESCRIPTION" => "N",
					"SET_META_KEYWORDS" => "N",
					"SET_STATUS_404" => "N",
					"SET_TITLE" => "N",
					"SHOW_404" => "N",
					"SORT_BY1" => "SORT",
					"SORT_BY2" => "ACTIVE_FROM",
					"SORT_ORDER1" => "ASC",
					"SORT_ORDER2" => "DESC"
				)
			);
		}
		return ob_get_clean();
	}
	function ShowLeftColumn() {
		global $APPLICATION;
		$APPLICATION->IncludeFile(GetDirPath($_SERVER['REAL_FILE_PATH']).'/left_col.php', array(), array('NAME' => 'Левая колонка', "TEMPLATE" => "left_col.php"));
	}
}
?>