<?
define("INCLUDE_DIR", dirname(__FILE__) . "/pos-brand/");
define("INCLUDE_DIR_REL", str_replace($_SERVER['DOCUMENT_ROOT'], '', dirname(__FILE__)) . "/pos-brand/");
define("LAYOUT_DIR", "/local/templates/pos-brand/_html/");
define("PROJECT_NAME", "POS Brand");

CModule::IncludeModule('nologostudio.main');

include INCLUDE_DIR.'CTemplate.php';
\Bitrix\Main\Loader::registerAutoLoadClasses(null,  array(
	'\PosBrand\CTemplate' => INCLUDE_DIR_REL.'CTemplate.php',
));

require_once INCLUDE_DIR . "functions.php";
require_once INCLUDE_DIR . "CModificationColumnsProperty.php";
require_once INCLUDE_DIR . "CColorPhotosProperty.php";
AddEventHandler("iblock", "OnIBlockPropertyBuildList", array("CModificationColumnsProperty", "GetUserTypeDescription"));
AddEventHandler("iblock", "OnIBlockPropertyBuildList", array("CColorPhotosProperty", "GetUserTypeDescription"));

function NLSSettings_GetSettings() {
	return array(
		"TEMPLATE" => array(
			"NAME" => "Настройки шаблона",
			"VARS" => array(
				"pos_tpl_header_email" => array(
					"NAME" => "E-mail в шапке",
					"DEFAULT" => ""
				),
				"pos_tpl_header_phone" => array(
					"NAME" => "Телефон в шапке",
					"DEFAULT" => ""
				),
				"pos_tpl_footer_address" => array(
					"NAME" => "Адрес в подвале",
					"DEFAULT" => ""
				),
				"pos_tpl_footer_phone" => array(
					"NAME" => "Телефон в подвале",
					"DEFAULT" => ""
				),
				"pos_tpl_footer_email" => array(
					"NAME" => "E-mail в подвале",
					"DEFAULT" => ""
				),
				"pos_tpl_empty_pic" => array(
					"NAME" => "Заглушка",
					"TYPE" => "FILE",
					"DEFAULT" => ""
				),
			)
		),
		"MAIN" => array(
			"NAME" => "Главная страница",
			"VARS" => array(
				"pos_main_advantages_title" => array(
					"NAME" => "Заголовок блока с преимуществами",
					"DEFAULT" => ""
				),
				"pos_main_clients_title" => array(
					"NAME" => "Заголовок блока с клиентами",
					"DEFAULT" => ""
				),
				"pos_main_photo_title" => array(
					"NAME" => "Заголовок блока с фотографиями",
					"DEFAULT" => ""
				),
				"pos_main_certs_title" => array(
					"NAME" => "Заголовок блока с сертификатами",
					"DEFAULT" => ""
				),
				"pos_main_dealers_title" => array(
					"NAME" => "Заголовок блока с дилерами",
					"DEFAULT" => ""
				),
			)
		),
		"CONTACTS" => array(
			"NAME" => "Страница 'Контакты'",
			"VARS" => array(
				"pos_contacts_address" => array(
					"NAME" => "Адрес",
					"DEFAULT" => ""
				),
				"pos_contacts_worktime" => array(
					"NAME" => "Режим работы",
					"DEFAULT" => ""
				),
				"pos_contacts_phone" => array(
					"NAME" => "Телефоны",
					"DEFAULT" => ""
				),
				"pos_contacts_email" => array(
					"NAME" => "E-mail",
					"DEFAULT" => ""
				),
				"pos_contacts_maplink" => array(
					"NAME" => "Ссылка на Яндекс.Карту",
					"DEFAULT" => ""
				),
			)
		),
		"ITEM_CARD" => array(
			"NAME" => "Карточка товара",
			"VARS" => array(
				"pos_item_card_phone" => array(
					"NAME" => "Телефон",
					"DEFAULT" => ""
				),
			)
		),
	);
}
?>