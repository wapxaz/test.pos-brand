<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
?> <?=$APPLICATION->AddBufferContent(Array('\PosBrand\CTemplate', 'ShowPostContent'))?>
<div class="footer-spacer">
</div>
 <footer class="footer">
<div class="container">
 <aside class="footer-top row"> <section class="footer_contact-info col-md-7 col-sm-7 col-xs-6">
	<p class="footer_contact-info_title">
		Контакты
	</p>
	<div class="footer_contact-info_contacts">
		 <?if(CNLSMainSettings::GetSiteSetting('pos_tpl_footer_address')):?> <?=CNLSMainSettings::GetSiteSetting('pos_tpl_footer_address')?> <br>
		 <?endif?> <?if(CNLSMainSettings::GetSiteSetting('pos_tpl_footer_phone')):?> тел. <?=CNLSMainSettings::GetSiteSetting('pos_tpl_footer_phone')?> <br>
		 <?endif?> <?if(CNLSMainSettings::GetSiteSetting('pos_tpl_footer_email')):?> E-mail: <a href="mailto:<?=CNLSMainSettings::GetSiteSetting('pos_tpl_footer_email')?>"><?=CNLSMainSettings::GetSiteSetting('pos_tpl_footer_email')?></a>
		<?endif?>
	</div>
 </section> <section class="footer_order-call col-md-5 col-sm-5 col-xs-6"> <a href="#order-call" onclick="openPopup(this);" class="footer_btn">Заказать звонок</a> </section> </aside>
	<?$APPLICATION->IncludeComponent(
	"bitrix:menu",
	"bottom",
	Array(
		"ALLOW_MULTI_SELECT" => "N",
		"CHILD_MENU_TYPE" => "left",
		"DELAY" => "N",
		"MAX_LEVEL" => "1",
		"MENU_CACHE_GET_VARS" => array(""),
		"MENU_CACHE_TIME" => "3600",
		"MENU_CACHE_TYPE" => "A",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"ROOT_MENU_TYPE" => "bottom",
		"USE_EXT" => "N"
	)
);?> <aside class="footer-bottom row">
	<?$APPLICATION->IncludeComponent(
	"bitrix:menu",
	"footer",
	Array(
		"ALLOW_MULTI_SELECT" => "N",
		"CHILD_MENU_TYPE" => "left",
		"DELAY" => "N",
		"MAX_LEVEL" => "1",
		"MENU_CACHE_GET_VARS" => array(""),
		"MENU_CACHE_TIME" => "3600",
		"MENU_CACHE_TYPE" => "A",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"ROOT_MENU_TYPE" => "footer",
		"USE_EXT" => "Y"
	)
);?>
	<p class="development">
		Разработка сайта:<a href="http://vefound.com/">veFound.com</a>
	</p>
 </aside>
</div>
 </footer>
<div style="display: none">
	<div class="popup" id="order-call">
 <span class="popup-close arcticmodal-close"></span>
		<?$APPLICATION->IncludeComponent(
	"bitrix:form.result.new",
	"become-dealer-form",
	Array(
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "A",
		"CHAIN_ITEM_LINK" => "",
		"CHAIN_ITEM_TEXT" => "",
		"COMPONENT_TEMPLATE" => "become-dealer-form",
		"EDIT_URL" => "",
		"IGNORE_CUSTOM_TEMPLATE" => "Y",
		"LIST_URL" => "",
		"SEF_FOLDER" => "/",
		"SEF_MODE" => "Y",
		"SUCCESS_URL" => "",
		"USE_EXTENDED_ERRORS" => "N",
		"WEB_FORM_ID" => "2"
	)
);?>
	</div>
</div>
	<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
	<script src="<?=LAYOUT_DIR?>/js/bootstrap.min.js"></script>
	<script src="<?=LAYOUT_DIR?>/js/jquery.arcticmodal-0.3.min.js"></script>
	<script src="<?=LAYOUT_DIR?>/js/jquery.fancybox.js?v=2.1.5" type="text/javascript" ></script>
	<script src="<?=LAYOUT_DIR?>/js/script.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('a[rel=honor]').fancybox({
				padding		: 0,
				prevEffect : 'fade',
				nextEffect : 'fade',
				helpers : {
					overlay : {
						css : {
							'background' : 'rgba(0, 0, 0, 0.7)'
						}
					},
					title : {
						type : 'inside'
					},
					buttons	: {},
					preload   : true
				},
				afterLoad : function() {
					this.title = '<span class="fancybox-title-over">' + '<span class="fancybox-title-pagination">' + (this.index + 1) + ' of ' + this.group.length + '</span>' + '<span class="fancybox-title-text">' + (this.title ? '' + this.title : '') + '</span>' + '</span>';
				}
			});
		});
		$('a[rel=honor]').eq(0).trigger('click');
	</script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('a[rel=production]').fancybox({
				padding		: 0,
				prevEffect : 'fade',
				nextEffect : 'fade',
				helpers : {
					overlay : {
						css : {
							'background' : 'rgba(0, 0, 0, 0.7)'
						}
					},
					title : {
						type : 'inside'
					},
					buttons	: {},
					preload   : true
				},
				afterLoad : function() {
					this.title = '<span class="fancybox-title-over">' + '<span class="fancybox-title-pagination">' + (this.index + 1) + ' of ' + this.group.length + '</span>' + '<span class="fancybox-title-text">' + (this.title ? '' + this.title : '') + '</span>' + '</span>';
				}
			});
		});
		$('a[rel=production]').eq(0).trigger('click');
	</script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('a[rel=certificates]').fancybox({
				padding		: 0,
				prevEffect : 'fade',
				nextEffect : 'fade',
				helpers : {
					overlay : {
						css : {
							'background' : 'rgba(0, 0, 0, 0.7)'
						}
					},
					title : {
						type : 'inside'
					},
					buttons	: {},
					preload   : true
				},
				afterLoad : function() {
					this.title = '<span class="fancybox-title-over">' + '<span class="fancybox-title-pagination">' + (this.index + 1) + ' of ' + this.group.length + '</span>' + '<span class="fancybox-title-text">' + (this.title ? '' + this.title : '') + '</span>' + '</span>';
				}
			});
		});
		$('a[rel=certificates]').eq(0).trigger('click');
	</script>


<!-- Yandex.Metrika counter --> <script type="text/javascript"> (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter40490375 = new Ya.Metrika({ id:40490375, clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks"); </script> <noscript><div><img src="https://mc.yandex.ru/watch/40490375" style="position:absolute; left:-9999px;" alt="" /></div></noscript> <!-- /Yandex.Metrika counter -->

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-86448463-1', 'auto');
  ga('send', 'pageview');
</script>