<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
use PosBrand;
?>
<!DOCTYPE html>
<html lang="ru">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, height=device-height">
	<meta name="viewport" content="width=device-width">
	<meta charset="utf-8">
	<?$APPLICATION->ShowHead();?>
	<title><?$APPLICATION->ShowTitle();?></title>
	<link href="<?=LAYOUT_DIR?>/img/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon">
	<link href="<?=LAYOUT_DIR?>/css/bootstrap.min.css" rel="stylesheet" media="all">
	<link href="<?=LAYOUT_DIR?>/css/owl.carousel.css" rel="stylesheet">
	<link href="<?=LAYOUT_DIR?>/css/owl.theme.css" rel="stylesheet">
	<link href="<?=LAYOUT_DIR?>/css/jquery.fancybox.css" rel="stylesheet" media="all">
	<link href="<?=LAYOUT_DIR?>/css/jquery.arcticmodal-0.3.css" rel="stylesheet" media="all">
	<link href="<?=LAYOUT_DIR?>/css/style.css" rel="stylesheet" media="all">
	<script src="<?=LAYOUT_DIR?>/js/jquery-2.1.3.min.js" type="text/javascript"></script>
	<script src="<?=LAYOUT_DIR?>/js/jquery.orbit.min.js" type="text/javascript"></script>
	<script src="<?=LAYOUT_DIR?>/js/owl.carousel.min.js"></script>
	<script type="text/javascript">
		var SITE_TEMPLATE_PATH = '<?=SITE_TEMPLATE_PATH?>';
		window.bluePlacemarks = new Array;
		window.filteredPlacemarks = new Array;
	</script>
</head>
<body>
	<div id="panel">
		<?$APPLICATION->ShowPanel();?>
	</div>
	<a href="#0" class="cd-top"></a>
	<div class="wrapper <?$APPLICATION->ShowProperty('page-type')?>">
		<header class="header">
			<div class="header-top container">
				<div class="row">
					<a href="/" class="header_logo-container col-md-6 col-sm-5 col-xs-6">
						<img src="<?=LAYOUT_DIR?>/img/logo.png" class="header_logo" alt="">
						<span class="header_logo-title">ПРОИЗВОДИТЕЛЬ POS-МАТЕРИАЛОВ</span>
					</a>
					<section class="header_contacts col-md-6 col-sm-7 col-xs-6">
						<div class="header_contacts_phone"><?=CNLSMainSettings::GetSiteSetting('pos_tpl_header_phone')?></div>
						<div class="header_contacts_contact">
							<a href="mailto:<?=CNLSMainSettings::GetSiteSetting('pos_tpl_header_email')?>"><?=CNLSMainSettings::GetSiteSetting('pos_tpl_header_email')?></a>
							<a href="#order-call" onclick="openPopup(this);">ЗАКАЗАТЬ ОБРАТНЫЙ ЗВОНОК</a>
						</div>
					</section>
				</div>
			</div>
			<nav class="main-menu navbar navbar-default" id="top_nav">
				<div class="container">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
							<span class="navbar-title">Меню</span>
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
					</div>
					<div class="row">
						<div class="navbar-collapse collapse">
<?$APPLICATION->IncludeComponent(
	"bitrix:menu",
	"top",
	Array(
		"ALLOW_MULTI_SELECT" => "N",
		"CHILD_MENU_TYPE" => "left",
		"DELAY" => "N",
		"MAX_LEVEL" => "1",
		"MENU_CACHE_GET_VARS" => array(""),
		"MENU_CACHE_TIME" => "3600",
		"MENU_CACHE_TYPE" => "A",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"ROOT_MENU_TYPE" => "top",
		"USE_EXT" => "N"
	)
);?>
						</div>
					</div>
				</div>
			</nav>
			<div id="menu_spacer" class="spacer"></div>
			<?=$APPLICATION->AddBufferContent(Array('\PosBrand\CTemplate', 'ShowSlider'))?>
		</header>
		<?=$APPLICATION->AddBufferContent(Array('\PosBrand\CTemplate', 'ShowPreContent'))?>