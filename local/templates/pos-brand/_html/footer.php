	<div class="footer-spacer"></div>
	<footer class="footer">
		<div class="container">
			<aside class="footer-top row">
				<section class="footer_contact-info col-md-7 col-sm-7 col-xs-6">
					<p class="footer_contact-info_title">Контакты</p>
					<div class="footer_contact-info_contacts">
						Московская обл., г. Балашиха, ш. Энтузиастов, влад. 1а <br>
						тел. 8-800-124-79-84 <br>
						E-mail: info@posbrand.ru
					</div>
				</section>
				<section class="footer_order-call col-md-5 col-sm-5 col-xs-6">
					<a href="#" class="footer_btn">Заказать звонок</a>
				</section>
			</aside>
			<aside class="footer-menu">
				<a href="#" class="footer-menu_link">ГЛАВНАЯ</a>
				<a href="#" class="footer-menu_link">КАТАЛОГ</a>
				<a href="#" class="footer-menu_link">О НАС</a>
				<a href="#" class="footer-menu_link">ПРОДУКЦИЯ</a>
				<a href="#" class="footer-menu_link">КАК СТАТЬ ДИЛЕРОМ</a>
				<a href="#" class="footer-menu_link">НАШИ КЛИЕНТЫ</a>
				<a href="#" class="footer-menu_link">КОНТАКТЫ</a>
			</aside>
			<aside class="footer-bottom row">
				<section class="footer_production col-md-3 col-sm-3 col-xs-6">
					<a class="footer_production-item" href="#">Карманы, подставки</a>
					<a class="footer_production-item" href="#">Рекламные аксессуары</a>
				</section>
				<section class="footer_production col-md-3 col-sm-3 col-xs-6">
					<a class="footer_production-item" href="#">Стойки для печатной продукции</a>
					<a class="footer_production-item" href="#">Зонтичные стенды (POP-up)</a>
				</section>
				<section class="footer_production col-md-3 col-sm-3 col-xs-6">
					<a class="footer_production-item" href="#">Рамочные стенды (Fold-up)</a>
					<a class="footer_production-item" href="#">Палеты из картона</a>
				</section>
				<section class="footer_production col-md-3 col-sm-3 col-xs-6">
					<a class="footer_production-item" href="#">Стойки металлические</a>
					<a class="footer_production-item" href="#">Промо-стойки, ресепшн</a>
				</section>
				<p class="development">Разработка сайта:<a href="http://vefound.com/">veFound.com</a></p>
			</aside>
		</div>
	</footer>
