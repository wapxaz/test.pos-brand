	<header class="header">
		<div class="header-top container">
			<div class="row">
				<a href="#" class="header_logo-container col-md-6 col-sm-5 col-xs-6">
					<img src="img/logo.png" class="header_logo" alt="">
					<span class="header_logo-title">ПРОИЗВОДИТЕЛЬ POS-МАТЕРИАЛОВ</span>
				</a>
				<section class="header_contacts col-md-6 col-sm-7 col-xs-6">
					<div class="header_contacts_phone">8-800-123-74-58</div>
					<div class="header_contacts_contact">
						<a href="#">INF0@POSBRAND.RU</a>
						<a href="#">ЗАКАЗАТЬ ОБРАТНЫЙ ЗВОНОК</a>
					</div>
				</section>
			</div>
		</div>
		<nav class="main-menu navbar navbar-default" id="top_nav">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="navbar-title">Меню</span>
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
				</div>
				<div class="row">
					<div class="navbar-collapse collapse">
					<div class="main-menu-container nav navbar-nav">
						<a class="main-menu_link main-menu_link_active" href="#">ГЛАВНАЯ</a>
						<a class="main-menu_link" href="#">КАТАЛОГ</a>
						<a class="main-menu_link" href="#">О НАС</a>
						<a class="main-menu_link" href="#">ПРОДУКЦИЯ</a>
						<a class="main-menu_link" href="#">КАК СТАТЬ ДИЛЕРОМ</a>
						<a class="main-menu_link" href="#">НАШИ КЛИЕНТЫ</a>
						<a class="main-menu_link" href="#">КОНТАКТЫ</a>
					</div>
				</div>
				</div>
			</div>
		</nav>
		<div id="menu_spacer" class="spacer"></div>
		<section id="slider" class="slider">
			<div class="slider-container">
			<span class="slider_info-container slider_info-container_hidden">
				<span class="slider_info-container_text">
					<span class="slider-container_title">производство и поставка <br>рекламных конструкций</span>
					<span class="slider-container_text">Возможен какой-то краткий текст</span>
					<a href="#" class="main-btn">Подробнее</a>
				</span>
			</span>
				<img src="img/header-bg1.jpg" alt="" />
			</div>
			<div class="slider-container">
			<span class="slider_info-container slider_info-container_hidden">
				<span class="slider_info-container_text">
					<span class="slider-container_title">Широкоформатная печать <br>в Калининграде</span>
					<span class="slider-container_text">Возможен какой-то краткий текст</span>
					<a href="#" class="main-btn">Подробнее</a>
				</span>
			</span>
				<img src="img/header-bg2.jpg" alt="" />
			</div>
			<div class="slider-container">
			<span class="slider_info-container slider_info-container_hidden">
				<span class="slider_info-container_text">
					<span class="slider-container_title">Автомобильный бизнес</span>
					<span class="slider-container_text">Возможен какой-то краткий текст</span>
					<a href="#" class="main-btn">Подробнее</a>
				</span>
			</span>
				<img src="img/header-bg3.jpg" alt="" />
			</div>
		</section>
	</header>