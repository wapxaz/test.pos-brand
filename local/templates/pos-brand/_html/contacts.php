<!DOCTYPE html>
<html lang="ru">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, height=device-height">
	<meta name="viewport" content="width=device-width">
	<meta charset="utf-8">
	<title>ПРОИЗВОДИТЕЛЬ POS-МАТЕРИАЛОВ</title>
	<link rel="shortcut icon" href="img/favicon.ico" type="image/vnd.microsoft.icon">
	<link href="css/bootstrap.min.css" rel="stylesheet" media="all">
	<link rel="stylesheet" href="css/owl.carousel.css">
	<link rel="stylesheet" href="css/owl.theme.css">
	<link href="css/jquery.fancybox.css" rel="stylesheet" media="all">
	<link href="css/style.css" rel="stylesheet" media="all">
</head>
<body>
<a href="#0" class="cd-top"></a>
<div class="wrapper">
	<?php
	include 'header.php';
	?>
	<article class="main">
		<div class="container">
			<section class="breadcrumbs">
				<a href="index.html" class="breadcrumb-prev">Главная</a>
				<a href="#" class="breadcrumb-current">Контакты</a>
			</section>
			<h1 class="main_title">Контакты</h1>
			<section class="contacts">
				<div class="contacts_block">
					<div class="contacts_block_item">
						<div class="contacts_block_item_title">АДРЕС</div>
						<div class="contacts_block_item_info">125430, Московская область, г. Реутов, ул. Фабричная, дом 6</div>
					</div>
					<div class="contacts_block_item">
						<div class="contacts_block_item_title">ГРАФИК РАБОТЫ</div>
						<div class="contacts_block_item_info">пн.-пт.: с 08:00 до 17:00</div>
					</div>
					<div class="contacts_block_item">
						<div class="contacts_block_item_title">ТЕЛЕФОНЫ</div>
						<div class="contacts_block_item_info">+7 499 136 4796 <br>+7 968 657 5759
						</div>
					</div>
					<div class="contacts_block_item">
						<div class="contacts_block_item_title">ПОЧТА</div>
						<div class="contacts_block_item_info">info@pos-brand.ru</div>
					</div>
				</div>
				<div class="contacts_block_text">Убедительная просьба обязательно предупредить менеджера, ведущего заказ о своим приезде заранее. Спасибо и надеемся на взаимопонимание!</div>
				<div class="contacts_map">
					<script type="text/javascript" charset="utf-8" src="https://api-maps.yandex.ru/services/constructor/1.0/js/?sid=itsGaAj743CFa6kqnRmAJalprE6qkO5h&width=100%&height=450&lang=ru_RU&sourceType=constructor"></script>
				</div>
			</section>
		</div>
	</article>
	<?php
	include 'footer.php';
	?>
</div>
<script src="js/jquery-2.1.3.min.js" type="text/javascript"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/script.js"></script>
</body>
</html>