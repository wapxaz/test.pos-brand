window.preloadedImages = new Array();
window.preloaded = 0;
function preload(img, callback) {
	for (i = 0; i < img.length; i++) {
		preloadedImages[i] = new Image();
		preloadedImages[i].src = img[i];
		preloadedImages[i].onload = function () {
			if(callback) {
				callback(img[i]);
			}
		}
	}
}
function preloadFromData(img, callback) {
	for (i = 0; i < img.length; i++) {
		preloadedImages[i] = new Image();
		preloadedImages[i].src = $(img[i]).data('src');
		preloadedImages[i].index = i;
		preloadedImages[i].onload = function () {
			if(callback) {
				callback($(img[this.index]));
			}
		}
	}
}
jQuery(document).ready(function($){
	runMenuCollapseLogic();

	var offset = 300,
		offset_opacity = 1200,
		scroll_top_duration = 700,
		$back_to_top = $('.cd-top');

	$(window).scroll(function(){
		if ($(this).scrollTop() > offset) {
			$back_to_top.addClass('cd-is-visible');
		} else {
			$back_to_top.removeClass('cd-is-visible cd-fade-out');
		}
		if( $(this).scrollTop() > offset_opacity ) { 
			$back_to_top.addClass('cd-fade-out');
		}
	});

	$back_to_top.click(function(event){
		event.preventDefault();
		$('body,html').animate({
			scrollTop: 0
			}, scroll_top_duration
		);
	});



	$(window).load(function() {
		$('.images-slider').each(function() {
			var sync1 = $(this),
				sync2 = $($(this).data('thumbnail-id')),
				cont = $('.images-thumbnail[data-color-id=' + sync1.data('color-id') + ']');

			sync1.owlCarousel({
				singleItem : true,
				slideSpeed : 1000,
				navigation: true,
				pagination:false,
				afterAction : syncPosition,
				responsiveRefreshRate : 200,
			});

			sync2.owlCarousel({
				items : 4,
				itemsTablet       : [768,3],
				itemsMobile       : [479,3],
				pagination:false,
				responsiveRefreshRate : 100,
				afterInit : function(el){
					el.find(".owl-item").eq(0).addClass("synced");
				}
			});

			$(".images-slider_arr_r", cont).on('click', function(){
				$(sync1).trigger('owl.next');
			});
			$(".images-slider_arr_l", cont).on('click', function(){
				$(sync1).trigger('owl.prev');
			});

			function syncPosition(el){
				var current = this.currentItem;
				$(sync2)
						.find(".owl-item")
						.removeClass("synced")
						.eq(current)
						.addClass("synced")
				if($(sync2).data("owlCarousel") !== undefined){
					center(current)
				}
			}

			$(sync2).on("click", ".owl-item", function(e){
				e.preventDefault();
				var number = $(this).data("owlItem");
				sync1.trigger("owl.goTo",number);
			});

			function center(number){
				var sync2visible = sync2.data("owlCarousel").owl.visibleItems;
				var num = number;
				var found = false;
				for(var i in sync2visible){
					if(num === sync2visible[i]){
						var found = true;
					}
				}

				if(found===false){
					if(num>sync2visible[sync2visible.length-1]){
						sync2.trigger("owl.goTo", num - sync2visible.length+2)
					}else{
						if(num - 1 === -1){
							num = 0;
						}
						sync2.trigger("owl.goTo", num);
					}
				} else if(num === sync2visible[sync2visible.length-1]){
					sync2.trigger("owl.goTo", sync2visible[1])
				} else if(num === sync2visible[0]){
					sync2.trigger("owl.goTo", num-1)
				}

			}
			
		});


		$('#clients-slider').owlCarousel({
			items : 4,
			itemsCustom : false,
			itemsDesktop : [1199,4],
			itemsDesktopSmall : [974,4],
			itemsTablet: [750,3],
			itemsTabletSmall: false,
			itemsMobile : [463,1],
			afterInit : function (slider) {
				if(this.itemsAmount <= this.visibleItems.length) {
					$(".client.default_slider_arr_r").hide();
					$(".client.default_slider_arr_l").hide();
				}
				$('#clients-slider .owl-wrapper').css('width', this.itemsAmount * parseInt($('#clients-slider .owl-item').width()));
			}
		});
		$(".client.default_slider_arr_r").click(function(){
			$("#clients-slider").trigger('owl.next');
		})
		$(".client.default_slider_arr_l").click(function(){
			$("#clients-slider").trigger('owl.prev');
		})

		$('#prod-slider').owlCarousel({
			items: 4,
			itemsDesktopSmall : [974,4],
			itemsTablet: [750,2],
			itemsMobile : [463,1],
			afterInit : function (slider) {
				if(this.itemsAmount <= this.visibleItems.length) {
					$(".prod.default_slider_arr_r").hide();
					$(".prod.default_slider_arr_l").hide();
				}
				$('#prod-slider .owl-wrapper').css('width', this.itemsAmount * parseInt($('#prod-slider .owl-item').width()));
			}
		});
		$(".prod.default_slider_arr_r").click(function(){
			$("#prod-slider").trigger('owl.next');
		})
		$(".prod.default_slider_arr_l").click(function(){
			$("#prod-slider").trigger('owl.prev');
		})

		$('#honor-slider').owlCarousel({
			items: 4,
			itemsDesktopSmall : [974,4],
			itemsTablet: [750,2],
			itemsMobile : [463,1],
			afterInit : function (slider) {
				if(this.itemsAmount <= this.visibleItems.length) {
					$(".honor.default_slider_arr_r").hide();
					$(".honor.default_slider_arr_l").hide();
				}
				$('#honor-slider .owl-wrapper').css('width', this.itemsAmount * parseInt($('#honor-slider .owl-item').width()));
			}
		});
		$(".honor.default_slider_arr_r").click(function(){
			$("#honor-slider").trigger('owl.next');
		})
		$(".honor.default_slider_arr_l").click(function(){
			$("#honor-slider").trigger('owl.prev');
		})

		$('#images-slider').owlCarousel({
			items : 1,
			itemsCustom : false,
			itemsDesktop : [1199,1],
			itemsDesktopSmall : [974,1],
			itemsTablet: [750,1],
			itemsTabletSmall: false,
			itemsMobile : [463,1],
		});
		$("#images-slider_thumbnail .images-slider_arr_r").click(function(){
			$("#images-slider").trigger('owl.next');
		});
		$("#images-slider_thumbnail .images-slider_arr_l").click(function(){
			$("#images-slider").trigger('owl.prev');
		});

		$('#sidebar-catalog-menu').click(function(){
			if($(this).hasClass('dropdown'))
			{
				var sidebarMenu = $('#sidebar-catalog-menu-list');
				if(sidebarMenu.hasClass('list-open'))
				{
					sidebarMenu.css('height', '');
					sidebarMenu.removeClass('list-open');
				}
				else
				{
					sidebarMenu.css('height', sidebarMenu.attr('data-h'));
					sidebarMenu.addClass('list-open');
				}
			}
		});


		$('#slider').orbit({
			'bullets': false,
			'timer' : true,
			'animation': 'fade',
			'advanceSpeed': 3000
		});

		$(".js-change-color").on('click', function(){
			$(this).addClass('product_colors_item_active').siblings().removeClass('product_colors_item_active');
			$('.images-slider:not(.hidden-block)').addClass('hidden-block');
			$('.images-thumbnail:not(.hidden-block)').addClass('hidden-block');
			$('.images-slider[data-color-id=' + $(this).data('color-id') + ']').removeClass('hidden-block');
			$('.images-thumbnail[data-color-id=' + $(this).data('color-id') + ']').removeClass('hidden-block');
			if($('.images-thumbnail[data-color-id=' + $(this).data('color-id') + '] .product_images_thumbnail').length) {
				$('.images-thumbnail[data-color-id=' + $(this).data('color-id') + ']').removeClass('hidden-block');
			}
		});

		$('.js-ajax-form-container form').on('submit', function (e) {
			e.preventDefault();
			$.ajax({
				data: $(this).serialize()+'&ajax=Y',
				url: $(this).parents('.js-ajax-form-container').data('href'),
				type: 'POST',
				context: $(this).parents('.js-ajax-form-container'),
				success: function (data) {
					$(this).replaceWith(data);
				}
			});
		});

		$('.js-showall-dealer').on('click', function (e) {
			e.preventDefault();
			$('.js-hide-dealer').val('').change();
		});

		$('.js-hide-dealer').on('change', function (e) {
			e.preventDefault();
			var val = $(this).val();
			if(val) {
				$('.contacts_block_item:not(.js-not-hide)').removeClass('hidden-block').addClass('need-hide');
				$('.contacts_block_item[data-region*="' + val + '"]').removeClass('need-hide');
				$('.contacts_block_item.need-hide').addClass('hidden-block').removeClass('need-hide');
			} else {
				$('.contacts_block_item:not(.js-not-hide)').removeClass('hidden-block');
			}

			renewMap(val);
		});
	});

	var headerHeight = $('.header-top:first').outerHeight();

	setTimeout(function(){
		headerHeight = $('.header-top:first').outerHeight();
	}, 200);

	function runMenuCollapseLogic()
	{
		if($(window).width() < 751) {

			var sidebar = $('#sidebar-catalog-menu-list');
			if(sidebar.outerHeight() > 1)
			{
				sidebar.attr('data-h', sidebar.outerHeight());
			}

			$('#sidebar-catalog-menu').addClass('dropdown');
			sidebar.addClass('dropdown');
		}
		if($(window).width() >= 751) {
			$('#sidebar-catalog-menu-list').css('height', '').removeClass('list-open');
			$('#sidebar-catalog-menu-list, #sidebar-catalog-menu').removeClass('dropdown');
		}
	}

	function runStickLogic()
	{
		var elem = $('#top_nav');
		var top = $(window).scrollTop();

		if (top  < headerHeight) {
			elem.removeClass('main-menu_fixed ');
			elem.css({
				position: '',
				top: ''
			});

			$('#menu_spacer').hide();

		} else {

			$('#menu_spacer').show();
			elem.addClass('main-menu_fixed');
			elem.css({
				position: 'fixed',
				top: 0
			});
		}
	}

	function throttle(func, ms)
	{

		var isThrottled = false,
			savedArgs,
			savedThis;

		function wrapper()
		{

			if (isThrottled) {
				savedArgs = arguments;
				savedThis = this;
				return;
			}

			func.apply(this, arguments);

			isThrottled = true;

			setTimeout(function ()
			{
				isThrottled = false;
				if (savedArgs) {
					wrapper.apply(savedThis, savedArgs);
					savedArgs = savedThis = null;
				}
			}, ms);
		}

		return wrapper;
	}

	var scrollListener = throttle(function(){
		runStickLogic();
	}, 20);

	var resizeListener = throttle(function (){
		headerHeight = $('.header:first').outerHeight();
		runStickLogic();
		runMenuCollapseLogic();
	}, 20);

	$(window).scroll(scrollListener);
	$(window).resize(resizeListener);

	if($('#map').length) {
		function initYMap() {
			window.dealerMap = new ymaps.Map("map", {
				center: [55, 80],
				controls: ['zoomControl',  'fullscreenControl'],
				zoom: 3
			});

			window.dealerClusterer = new ymaps.Clusterer({
				preset: 'islands#invertedNightClusterIcons'
			});
			dealerMap.geoObjects.add(dealerClusterer);
			renewMap('');
		}
		function renewMap(val) {
			var k = 0;
			filteredPlacemarks = new Array;
			minX = maxX = bluePlacemarks[0].coord_x;
			minY = maxY = bluePlacemarks[0].coord_y;
			for (var i = bluePlacemarks.length - 1; i >= 0; i--) {
				if(val == '' || bluePlacemarks[i].XML_ID.indexOf(val) != -1) {
					filteredPlacemarks[k] = bluePlacemarks[i];
					k++;
					minX = Math.min(minX, bluePlacemarks[i].coord_x);
					maxX = Math.max(maxX, bluePlacemarks[i].coord_x);
					minY = Math.min(minY, bluePlacemarks[i].coord_y);
					maxY = Math.max(maxY, bluePlacemarks[i].coord_y);
				}
			}
			if(filteredPlacemarks.length > 1) {
				dealerMap.setBounds([[minX,minY], [maxX,maxY]]);
			} else {
				dealerMap.setCenter([minX,minY], 13);
			}

			minX = maxX = filteredPlacemarks[0].coord_x;
			minY = maxY = filteredPlacemarks[0].coord_y;
			for (var i = filteredPlacemarks.length - 1; i >= 0; i--) {
				if(filteredPlacemarks[i].XML_ID && filteredPlacemarks[i].XML_ID.indexOf(val) != -1) {
					minX = Math.min(minX, filteredPlacemarks[i].coord_x);
					maxX = Math.max(maxX, filteredPlacemarks[i].coord_x);
					minY = Math.min(minY, filteredPlacemarks[i].coord_y);
					maxY = Math.max(maxY, filteredPlacemarks[i].coord_y);
				}
			}
			if(filteredPlacemarks.length > 1 && minX != maxX && minY != maxY) {
				dealerMap.setBounds([[minX,minY], [maxX,maxY]]);
			} else {
				dealerMap.setCenter([minX,minY], 13);
			}

			dealerClusterer.removeAll();
			dealerClusterer.add(createGeoObjects(filteredPlacemarks));
		}

		ymaps.ready(function () {
			initYMap();
		});


		function fillCollection(dealerMap, collection, placemarks)
		{
			for(var i=0; i < placemarks.length; i++)
			{
				collection.add(createGeoObject(placemarks[i]['coord_x'], placemarks[i]['coord_y'], dealerMap, placemarks[i]['iconContent'], placemarks[i]['hintContent'], placemarks[i]['balloonContentHeader'], placemarks[i]['balloonContentBody']));
			}

			return collection;
		}

		function createCollectionBlue()
		{
			return new ymaps.GeoObjectCollection({}, {
				iconLayout: 'default#image',
				iconImageHref: SITE_TEMPLATE_PATH + '/_html/img/placemark.png',
				iconImageSize: [24, 49],
				iconImageOffset: [0, -49],
				draggable: false
			});
		}

		function createGeoObject(x, y, dealerMap, title, hint, head, cont)
		{
			var geoObject = new ymaps.Placemark([x, y], {
				hintContent: hint,
				balloonContentHeader: head,
				balloonContentBody: cont
			}, {
				iconLayout: 'default#image',
				iconImageHref: SITE_TEMPLATE_PATH + '/_html/img/placemark.png',
				iconImageSize: [24, 49],
				iconImageOffset: [0, -49],
				draggable: false
			});

			return geoObject;
		}

		function createGeoObjects(placemarks)
		{
			var arPlacemarks = new Array;
			for(var i=0; i < placemarks.length; i++)
			{
				arPlacemarks[i] = createGeoObject(placemarks[i]['coord_x'], placemarks[i]['coord_y'], dealerMap, placemarks[i]['iconContent'], placemarks[i]['hintContent'], placemarks[i]['balloonContentHeader'], placemarks[i]['balloonContentBody']);
			}

			return arPlacemarks;
		}
	}

	if($('.hidden-block img').length) {
		var img = new Array,
			i = 0;
		$('.hidden-block img').each(function () {
			img[i] = $(this).attr('src');
			i++;
		});
		preload(img);
	}

	if($('.preload-banner-image').length) {
		var img = new Array;
		img[0] = $('.preload-banner-image:eq(0)');
		function bannerCallback(img) {
			$(img).attr('src', $(img).data('src'));
			$('.slider_info-container[data-index="' + $(img).data('index') + '"]').removeClass('slider_info-container_hidden');
			$('.slider-container[data-index="' + $(img).data('index') + '"]').addClass('slider-container_visible');
			if(window.preloaded == 0) {
				var imgP = new Array,
					i = 0;
				$('.preload-banner-image:gt(0)').each(function () {
					imgP[i] = $(this);
					i++;
				});
				window.preloaded++;
				preloadFromData(imgP, bannerCallback);
			} else {
				$('.slider-container[data-index="' + $(img).data('index') + '"]').css('z-index', 0);
			}
		}
		preloadFromData(img, bannerCallback);
	}

	(function($) {
		$(function() {

			$('div.product_block_tabs').on('click', 'span.product_block_title:not(.active)', function() {
				$(this)
					.addClass('active').siblings().removeClass('active')
					.closest('div.product_tabs').find('div.product_tabs_content').removeClass('active').eq($(this).index()).addClass('active');
			});

		});
	})(jQuery);
});
function openPopup(link) {
	$($(link).attr('href')).arcticmodal();
}
