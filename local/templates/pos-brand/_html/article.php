<!DOCTYPE html>
<html lang="ru">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, height=device-height">
	<meta name="viewport" content="width=device-width">
	<meta charset="utf-8">
	<title>ПРОИЗВОДИТЕЛЬ POS-МАТЕРИАЛОВ</title>
	<link rel="shortcut icon" href="img/favicon.ico" type="image/vnd.microsoft.icon">
	<link href="css/bootstrap.min.css" rel="stylesheet" media="all">
	<link rel="stylesheet" href="css/owl.carousel.css">
	<link rel="stylesheet" href="css/owl.theme.css">
	<link href="css/style.css" rel="stylesheet" media="all">
</head>
<body>
<a href="#0" class="cd-top"></a>
<div class="wrapper">
	<?php
	include 'header.php';
	?>
	<article class="main">
		<div class="container">
			<section class="breadcrumbs">
			<a href="index.html" class="breadcrumb-prev">Главная</a>
			<a href="#" class="breadcrumb-prev">Статьи</a>
			<a href="#" class="breadcrumb-current">Статья 1</a>
		</section>
			<h1 class="main_title">статья (h1)</h1>
			<div class="main_text-block">
				<img class="main_img" src="img/def-img1.jpg" alt="">
				<h2 class="second-title">ЗАГОЛОВОК 2 (H2)</h2>
				<p class="main_text">
					Ресепшн уже давно стал частью полноценного офиса многих компаний. Это первое, что видят посетители, партнеры. Ресепшн № 1 – набор стоек, хорошо компонуется с разными моделями такой же гаммы. Отдельно приобретаются необходимые для работы секретарю элементы – тумбы, шкафы. Мебель смотрится легко, не загружает помещение визуально. Внешние фасады округленной конструкции из пластика, внутренняя отделка – ламинированная ДВП, кромка PVC защищает от механических повреждений. Внутри достаточно встроенных полок для канцелярских принадлежностей и бумаг.
				</p>
				<p class="main_text">
					Ресепшн уже давно стал частью полноценного офиса многих компаний. Это первое, что видят посетители, партнеры. Ресепшн № 1 – набор стоек, хорошо компонуется с разными моделями такой же гаммы. Отдельно приобретаются необходимые для работы секретарю элементы – тумбы, шкафы. Мебель смотрится легко, не загружает помещение визуально. Внешние фасады округленной конструкции из пластика, внутренняя отделка – ламинированная ДВП, кромка PVC защищает от механических повреждений. Внутри достаточно встроенных полок для канцелярских принадлежностей и бумаг.
				</p>
				<h3 class="third-title">ЗАГОЛОВОК 3 (H3)</h3>
				<p class="main_text">
					Ресепшн уже давно стал частью полноценного офиса многих компаний. Это первое, что видят посетители, партнеры. Ресепшн № 1 – набор стоек, хорошо компонуется с разными моделями такой же гаммы. Отдельно приобретаются необходимые для работы секретарю элементы – тумбы, шкафы. Мебель смотрится легко, не загружает помещение визуально. Внешние фасады округленной конструкции из пластика, внутренняя отделка – ламинированная ДВП, кромка PVC защищает от механических повреждений. Внутри достаточно встроенных полок для канцелярских принадлежностей и бумаг.
				</p>
				<h4 class="fourth-title">Пункты перечисления (H4):</h4>
				<ul class="main_ul">
					<li>Ресепшн уже давно стал частью полноценного офиса многих компаний. Это первое, что видят посетители, партнеры. Ресепшн № 1 – набор стоек, хорошо компонуется с разными моделями такой же гаммы.</li>
					<li>Ресепшн уже давно стал частью полноценного офиса многих компаний. Это первое, что видят посетители, партнеры. Ресепшн № 1 – набор стоек, хорошо компонуется с разными моделями такой же гаммы.</li>
					<li>Ресепшн уже давно стал частью полноценного офиса многих компаний. Это первое, что видят посетители, партнеры. Ресепшн № 1 – набор стоек, хорошо компонуется с разными моделями такой же гаммы.</li>
					<li>Ресепшн уже давно стал частью полноценного офиса многих компаний. Это первое, что видят посетители, партнеры. Ресепшн № 1 – набор стоек, хорошо компонуется с разными моделями такой же гаммы.</li>
				</ul>
				<h4 class="fourth-title">Пункты перечисления (H4):</h4>
				<ol class="main_ol">
					<li>Ресепшн уже давно стал частью полноценного офиса многих компаний. Это первое, что видят посетители, партнеры. Ресепшн № 1 – набор стоек, хорошо компонуется с разными моделями такой же гаммы.</li>
					<li>Ресепшн уже давно стал частью полноценного офиса многих компаний. Это первое, что видят посетители, партнеры. Ресепшн № 1 – набор стоек, хорошо компонуется с разными моделями такой же гаммы.</li>
					<li>Ресепшн уже давно стал частью полноценного офиса многих компаний. Это первое, что видят посетители, партнеры. Ресепшн № 1 – набор стоек, хорошо компонуется с разными моделями такой же гаммы.</li>
					<li>Ресепшн уже давно стал частью полноценного офиса многих компаний. Это первое, что видят посетители, партнеры. Ресепшн № 1 – набор стоек, хорошо компонуется с разными моделями такой же гаммы.</li>
				</ol>
			</div>
		</div>
	</article>
	<?php
	include 'footer.php';
	?>
</div>
<script src="js/jquery-2.1.3.min.js" type="text/javascript"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/script.js"></script>
</body>
</html>