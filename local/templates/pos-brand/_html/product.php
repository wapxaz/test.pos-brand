<!DOCTYPE html>
<html lang="ru">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, height=device-height">
	<meta name="viewport" content="width=device-width">
	<meta charset="utf-8">
	<title>ПРОИЗВОДИТЕЛЬ POS-МАТЕРИАЛОВ</title>
	<link rel="shortcut icon" href="img/favicon.ico" type="image/vnd.microsoft.icon">
	<link href="css/bootstrap.min.css" rel="stylesheet" media="all">
	<link rel="stylesheet" href="css/owl.carousel.css">
	<link rel="stylesheet" href="css/owl.theme.css">
	<link href="css/jquery.fancybox.css" rel="stylesheet" media="all">
	<link href="css/style.css" rel="stylesheet" media="all">
</head>
<body>
<a href="#0" class="cd-top"></a>
<div class="wrapper">
	<?php
	include 'header.php';
	?>
	<div class="container">
		<div class="row">
			<div class="sidebar col-md-3 col-sm-4">
				<div id="sidebar-catalog-menu" class="sidebar_title">
					<span>каталог</span>
					<button type="button" class="navbar-toggle">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
				</div>
				<ul id="sidebar-catalog-menu-list" class="sidebar_menu-second">
					<li><a href="#" class="sidebar_menu-second_link">Карманы, подставки</a></li>
					<li><a href="#" class="sidebar_menu-second_link">Рекламные аксессуары</a></li>
					<li><a href="#" class="sidebar_menu-second_link">Стойки металлические</a></li>
					<li><a href="#" class="sidebar_menu-second_link">Стойки</a></li>
					<li><a href="#" class="sidebar_menu-second_link">Зонтичные стенды (POP-Up)</a></li>
					<li>
						<a href="#" class="sidebar_menu-second_link_drop-main">Промостойки, ресепшен</a>
						<ul href="#" class="sidebar_menu-second_link sidebar_menu-second_link_drop">
							<li><a href="#" class="sidebar_menu-second_link-inner">Промостойка № 1</a></li>
							<li><a href="#" class="sidebar_menu-second_link-inner">Промостойка № 2</a></li>
							<li><a href="#" class="sidebar_menu-second_link-inner">Промостойка № 3</a></li>
							<li><a href="#" class="sidebar_menu-second_link-inner">Промостойка № 4</a></li>
							<li><a href="#" class="sidebar_menu-second_link-inner sidebar_menu-second_link-inner_active">Ресепшн № 1</a></li>
							<li><a href="#" class="sidebar_menu-second_link-inner">Ресепшн № 2</a></li>
							<li><a href="#" class="sidebar_menu-second_link-inner">Ресепшн № 3</a></li>
							<li><a href="#" class="sidebar_menu-second_link-inner">Ресепшн № 4</a></li>
						</ul>
					</li>
					<li><a href="#" class="sidebar_menu-second_link">Рамочные стенды (FOLD-UP)</a></li>
					<li><a href="#" class="sidebar_menu-second_link">Паллеты из картона (POSM)</a></li>
					<li><a href="#" class="sidebar_menu-second_link">Многорамочные системы</a></li>
				</ul>
			</div>
			<article class="main col-md-9 col-sm-8">
				<section class="breadcrumbs">
					<a href="index.html" class="breadcrumb-prev">Главная</a>
					<a href="#" class="breadcrumb-prev">Каталог</a>
					<a href="#" class="breadcrumb-prev">Промостойки, ресепшн</a>
					<a href="#" class="breadcrumb-current">Ресепшн № 1</a>
				</section>
				<section class="product">
					<div class="product_images">
						<div class="images-slider" id="images-slider">
							<a href="img/product-img1.jpg" class="product_images_img" rel="image">
								<img src="img/product-img1.jpg" alt="">
							</a>
							<a href="img/catalog-img1.jpg" class="product_images_img" rel="image">
								<img src="img/catalog-img1.jpg" alt="">
							</a>
							<a href="img/catalog-img5.jpg" class="product_images_img" rel="image">
								<img src="img/catalog-img5.jpg" alt="">
							</a>
							<a href="img/catalog-img8.jpg" class="product_images_img" rel="image">
								<img src="img/catalog-img8.jpg" alt="">
							</a>
							<a href="img/catalog-img6.jpg" class="product_images_img" rel="image">
								<img src="img/catalog-img6.jpg" alt="">
							</a>
						</div>
						<div class="images-thumbnail">
							<span class="images-slider_arr images-slider_arr_l"></span>
							<span class="images-slider_arr images-slider_arr_r"></span>
							<div class="images-slider_thumbnail" id="images-slider_thumbnail">
								<a href="#" class="product_images_thumbnail">
									<img src="img/product-img1.jpg" alt="">
								</a>
								<a href="#" class="product_images_thumbnail">
									<img src="img/catalog-img1.jpg" alt="">
								</a>
								<a href="#" class="product_images_thumbnail">
									<img src="img/catalog-img5.jpg" alt="">
								</a>
								<a href="#" class="product_images_thumbnail">
									<img src="img/catalog-img8.jpg" alt="">
								</a>
								<a href="#" class="product_images_thumbnail">
									<img src="img/catalog-img6.jpg" alt="">
								</a>
							</div>
						</div>
					</div>
					<h1 class="main_title">ресепшн <span>№ 1</span></h1>
					<div class="product_descr">Ресепшн уже давно стал частью полноценного офиса многих компаний. Это первое, что видят посетители, партнеры. Ресепшн № 1 – набор стоек, хорошо компонуется с разными моделями такой же гаммы. Отдельно приобретаются необходимые для работы секретарю элементы – тумбы, шкафы.</div>
					<div class="product_colors">
						<div class="product_block_title">цвета</div>
						<span class="product_colors_item product_colors_item_active">
							<span class="product_colors_item_color" style="background: #000"></span>
							<span class="product_colors_item_title">черный</span>
						</span>
						<span class="product_colors_item">
							<span class="product_colors_item_color" style="background: #ffd531"></span>
							<span class="product_colors_item_title">желтый</span>
						</span>
						<span class="product_colors_item">
							<span class="product_colors_item_color" style="background: #4a3505"></span>
							<span class="product_colors_item_title">коричневый</span>
						</span>
						<span class="product_colors_item">
							<span class="product_colors_item_color" style="background: #0876bb"></span>
							<span class="product_colors_item_title">синий</span>
						</span>
						<span class="product_colors_item">
							<span class="product_colors_item_color" style="background: #f88b24"></span>
							<span class="product_colors_item_title">оранжевый</span>
						</span>
						<span class="product_colors_item">
							<span class="product_colors_item_color" style="background: #caba93"></span>
							<span class="product_colors_item_title">бежевый</span>
						</span>
						<span class="product_colors_item">
							<span class="product_colors_item_color" style="background: #13aae2"></span>
							<span class="product_colors_item_title">голубой</span>
						</span>
					</div>
					<div class="product_parameters">
						<table class="product_parameters_content">
							<tr class="product_block_title_container">
								<th class="product_block_title">артикул</th>
								<th class="product_block_title">возможные размеры, <span>см</span></th>
								<th class="product_block_title">вес, <span>кг</span></th>
								<th class="product_block_title">запах</th>
								<th class="product_block_title">состояние</th>
							</tr>
							<tr class="product_parameters_item">
								<td class="product_parameters_item_content">001/а</td>
								<td class="product_parameters_item_content">150 x 80 x 1000</td>
								<td class="product_parameters_item_content">15</td>
								<td class="product_parameters_item_content">клубника</td>
								<td class="product_parameters_item_content">жидкое</td>
							</tr>
							<tr class="product_parameters_item">
								<td class="product_parameters_item_content">002/b</td>
								<td class="product_parameters_item_content">180 x 100 x 1000</td>
								<td class="product_parameters_item_content">20</td>
								<td class="product_parameters_item_content">банан</td>
								<td class="product_parameters_item_content">твердое</td>
							</tr>
							<tr class="product_parameters_item">
								<td class="product_parameters_item_content">003/c</td>
								<td class="product_parameters_item_content">200 x 100 x 1000</td>
								<td class="product_parameters_item_content">35</td>
								<td class="product_parameters_item_content">яблоко</td>
								<td class="product_parameters_item_content">газообразное</td>
							</tr>
						</table>
					</div>
					<div class="product_tabs">
						<div class="product_block_title_container product_block_tabs">
							<span class="product_block_title active">описание</span>
							<span class="product_block_title">характеристики</span>
							<span class="product_block_title">сертификаты</span>
							<span class="product_block_title">видео</span>
							<span class="product_block_title">инструкция по сборке</span>
						</div>
						<div class="product_tabs_content active">

						</div>
						<div class="product_tabs_content">
							<h3 class="third-title">ОСНОВНЫЕ ХАРАКТЕРИСТИКИ</h3>
							<div class="product_tabs_content_item">
								<span class="product_tabs_content_item_title">ВИД:</span>
								<span class="product_tabs_content_item_content">настенный, настольный</span>
							</div>
							<div class="product_tabs_content_item">
								<span class="product_tabs_content_item_title">МАТЕРИАЛ:</span>
								<span class="product_tabs_content_item_content">алюминиевый профиль</span>
							</div>
							<div class="product_tabs_content_item">
								<span class="product_tabs_content_item_title">ШИРИНА ПРОФИЛЯ:</span>
								<span class="product_tabs_content_item_content">25 мм</span>
							</div>
							<div class="product_tabs_content_item">
								<span class="product_tabs_content_item_title">ПОД ЗАКАЗ ФОРМАТ:</span>
								<span class="product_tabs_content_item_content">100 х 70 см</span>
							</div>
						</div>
						<div class="product_tabs_content">
							<div class="product_certificates row">
								<a href="img/honor-img.jpg" class="col-md-3 col-sm-4 col-xs-3 product_certificates_item" rel="certificates">
									<span class="img-zoom">
										<img src="img/honor-img.jpg">
									</span>
								</a>
								<a href="img/honor-img.jpg" class="col-md-3 col-sm-4 col-xs-3 product_certificates_item" rel="certificates">
									<span class="img-zoom">
										<img src="img/honor-img.jpg">
									</span>
								</a>
								<a href="img/honor-img.jpg" class="col-md-3 col-sm-4 col-xs-3 product_certificates_item" rel="certificates">
									<span class="img-zoom">
										<img src="img/honor-img.jpg">
									</span>
								</a>
								<a href="img/honor-img.jpg" class="col-md-3 col-sm-4 col-xs-3 product_certificates_item" rel="certificates">
									<span class="img-zoom">
										<img src="img/honor-img.jpg">
									</span>
								</a>
								<a href="img/honor-img.jpg" class="col-md-3 col-sm-4 col-xs-3 product_certificates_item" rel="certificates">
									<span class="img-zoom">
										<img src="img/honor-img.jpg">
									</span>
								</a>
							</div>
						</div>
						<div class="product_tabs_content">
							<h3 class="third-title">МУЛЬТИМЕДИЙНЫЕ РЕКЛАМНЫЕ СТОЙКИ</h3>
							<iframe width="490" height="275" src=http://www.youtube.com/embed/DkaUsBwe0fo frameborder="0" allowfullscreen></iframe>
						</div>
						<div class="product_tabs_content">
							<h3 class="third-title">ИНСТРУКЦИИ ПО СБОРКЕ РАЗЛИЧНЫХ МОДЕЛЕЙ</h3>
							<div class="fs">
								<div class="col-md-6 col-sm-6 col-xs-6 product_instruction pdf">
									<span class="product_instruction_doc"></span>
									<span class="product_instruction_title">Инструкция по сборке модели 001/А черного цвета.</span>
									<span class="product_instruction_format">PDF (5,3 МБ)</span>
									<a href="#" class="product_instruction_download">СКАЧАТЬ</a>
								</div>
								<div class="col-md-6 col-sm-6 col-xs-6 product_instruction mkv">
									<span class="product_instruction_doc"></span>
									<span class="product_instruction_title">doc13</span>
									<span class="product_instruction_format">PDF (5,3 МБ)</span>
									<a href="#" class="product_instruction_download">СКАЧАТЬ</a>
								</div>
								<div class="col-md-6 col-sm-6 col-xs-6 product_instruction doc">
									<span class="product_instruction_doc"></span>
									<span class="product_instruction_title">Инструкция по сборке модели 001/А черного цвета.</span>
									<span class="product_instruction_format">PDF (5,3 МБ)</span>
									<a href="#" class="product_instruction_download">СКАЧАТЬ</a>
								</div>
								<div class="col-md-6 col-sm-6 col-xs-6 product_instruction png">
									<span class="product_instruction_doc"></span>
									<span class="product_instruction_title">Инструкция по сборке модели 001/А черного цвета.</span>
									<span class="product_instruction_format">PDF (5,3 МБ)</span>
									<a href="#" class="product_instruction_download">СКАЧАТЬ</a>
								</div>
							</div>
						</div>
					</div>
					<div class="product_questions">
						<div class="product_questions_title">ОСТАЛИСЬ ВОПРОСЫ?
							<span>ЗАДАЙТЕ ИХ НАШИМ СПЕЦИАЛИСТАМ!</span>
						</div>
						<div class="product_questions_phone">8-800-123-74-58
							<a href="#">ЗАКАЗАТЬ ОБРАТНЫЙ ЗВОНОК</a>
						</div>
					</div>
				</section>
			</article>
		</div>
	</div>
	<?php
	include 'footer.php';
	?>
</div>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="js/jquery-2.1.3.min.js" type="text/javascript"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.fancybox.js?v=2.1.5" type="text/javascript" ></script>
<script src="js/script.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		$('a[rel=image]').fancybox({
			padding		: 0,
			prevEffect : 'fade',
			nextEffect : 'fade',
			helpers : {
				overlay : {
					css : {
						'background' : 'rgba(0, 0, 0, 0.7)'
					}
				},
				title : {
					type : 'inside'
				},
				buttons	: {},
				preload   : true
			},
			afterLoad : function() {
				this.title = '<span class="fancybox-title-over">' + '<span class="fancybox-title-pagination">' + (this.index + 1) + ' of ' + this.group.length + '</span>' + '<span class="fancybox-title-text">' + (this.title ? '' + this.title : '') + '</span>' + '</span>';
			}
		});
	});
	$('a[rel=image]').eq(0).trigger('click');
</script>
<script type="text/javascript">
	$(document).ready(function() {
		$('a[rel=certificates]').fancybox({
			padding		: 0,
			prevEffect : 'fade',
			nextEffect : 'fade',
			helpers : {
				overlay : {
					css : {
						'background' : 'rgba(0, 0, 0, 0.7)'
					}
				},
				title : {
					type : 'inside'
				},
				buttons	: {},
				preload   : true
			},
			afterLoad : function() {
				this.title = '<span class="fancybox-title-over">' + '<span class="fancybox-title-pagination">' + (this.index + 1) + ' of ' + this.group.length + '</span>' + '<span class="fancybox-title-text">' + (this.title ? '' + this.title : '') + '</span>' + '</span>';
			}
		});
	});
	$('a[rel=certificates]').eq(0).trigger('click');
</script>
</body>
</html>