<!DOCTYPE html>
<html lang="ru">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, height=device-height">
	<meta name="viewport" content="width=device-width">
	<meta charset="utf-8">
	<title>ПРОИЗВОДИТЕЛЬ POS-МАТЕРИАЛОВ</title>
	<link rel="shortcut icon" href="img/favicon.ico" type="image/vnd.microsoft.icon">
	<script src="js/jquery-2.1.3.min.js" type="text/javascript"></script>
	<script src="js/jquery.orbit.min.js" type="text/javascript"></script>
	<script src="js/owl.carousel.min.js"></script>
	<link href="css/bootstrap.min.css" rel="stylesheet" media="all">
	<link rel="stylesheet" href="css/owl.carousel.css">
	<link rel="stylesheet" href="css/owl.theme.css">
	<link href="css/jquery.fancybox.css" rel="stylesheet" media="all">
	<link href="css/style.css" rel="stylesheet" media="all">
</head>
<body>
<div class="popup-mask" style="display: none">
	<div class="popup">
		<span class="popup-close"></span>
		<h2 class="main_title">Заказать звонок</h2>
		<form name="#" action="/">
			<div class="form_item">
				<label>Контактное лицо<span>*</span></label>
				<span class="form_item_input-container"><input type="text" required="required"></span>
			</div>
			<div class="form_item">
				<label>Телефон<span>*</span></label>
				<span class="form_item_input-container"><input type="text" required="required"></span>
			</div>
			<p class="form_item_p"><span class="form_item_p_imp">*</span> - все поля обязательны для заполнения</p>
			<div class="form_item_btn_container">
				<input class="form_item_btn" type="submit" value="Отправить">
			</div>
		</form>
	</div>
</div>
<a href="#0" class="cd-top"></a>
<div class="wrapper main-page">
	<?php
	include 'header.php';
	?>
	<article class="main">
		<section class="catalog">
			<h1 class="main_title">каталог</h1>
			<div class="container">
				<div class="row fs">
					<figure class="catalog_item col-md-3 col-sm-3 col-xs-6">
						<a href="#" class="catalog_item_content">
							<span class="catalog_item_title"><span class="catalog_item_title_content">рамочные стенды fold-up</span></span>
							<img src="img/catalog-img1.jpg" alt="">
						</a>
					</figure>
					<figure class="catalog_item col-md-3 col-sm-3 col-xs-6">
						<a href="#" class="catalog_item_content">
							<span class="catalog_item_title"><span class="catalog_item_title_content">стойки для печатной продукции</span></span>
							<img src="img/catalog-img2.jpg" alt="">
						</a>
					</figure>
					<figure class="catalog_item col-md-3 col-sm-3 col-xs-6">
						<a href="#" class="catalog_item_content">
							<span class="catalog_item_title"><span class="catalog_item_title_content">промостойки, ресепшн</span></span>
							<img src="img/catalog-img3.jpg" alt="">
						</a>
					</figure>
					<figure class="catalog_item col-md-3 col-sm-3 col-xs-6">
						<a href="#" class="catalog_item_content">
							<span class="catalog_item_title"><span class="catalog_item_title_content">Визуальная навигация</span></span>
							<img src="img/catalog-img4.jpg" alt="">
						</a>
					</figure>
					<figure class="catalog_item col-md-3 col-sm-3 col-xs-6">
						<a href="#" class="catalog_item_content">
							<span class="catalog_item_title"><span class="catalog_item_title_content">многорамочные системы</span></span>
							<img src="img/catalog-img5.jpg" alt="">
						</a>
					</figure>
					<figure class="catalog_item col-md-3 col-sm-3 col-xs-6">
						<a href="#" class="catalog_item_content">
							<span class="catalog_item_title"><span class="catalog_item_title_content">рекламные аксессуары</span></span>
							<img src="img/catalog-img6.jpg" alt="">
						</a>
					</figure>
					<figure class="catalog_item col-md-3 col-sm-3 col-xs-6">
						<a href="#" class="catalog_item_content">
							<span class="catalog_item_title"><span class="catalog_item_title_content">карманы, подставки</span></span>
							<img src="img/catalog-img7.jpg" alt="">
						</a>
					</figure>
					<figure class="catalog_item col-md-3 col-sm-3 col-xs-6">
						<a href="#" class="catalog_item_content">
							<span class="catalog_item_title"><span class="catalog_item_title_content">стойки металлические</span></span>
							<img src="img/catalog-img8.jpg" alt="">
						</a>
					</figure>
				</div>
			</div>
		</section>
		<section class="advantages">
			<h2 class="main_title">наши преимущества</h2>
			<div class="container">
				<div class="advantages-info">индивидуальный подход при проектировании каждой отдельной рекламной системы <span>posbrand</span></div>
				<div class="row fs">
					<figure class="advantages_item col-md-3 col-sm-3 col-xs-6">
						<img src="img/adv-img1.png" alt="">
						<figcaption class="advantages_item_title"><span class="advantages_item_title_content">Широкий ассортимент, позволяющий решать разные задачи</span></figcaption>
					</figure>
					<figure class="advantages_item col-md-3 col-sm-3 col-xs-6">
						<img src="img/adv-img2.png" alt="">
						<figcaption class="advantages_item_title"><span class="advantages_item_title_content">Высокое качество</span></figcaption>
					</figure>
					<figure class="advantages_item col-md-3 col-sm-3 col-xs-6">
						<img src="img/adv-img3.png" alt="">
						<figcaption class="advantages_item_title"><span class="advantages_item_title_content">Долговечность</span></figcaption>
					</figure>
					<figure class="advantages_item col-md-3 col-sm-3 col-xs-6">
						<img src="img/adv-img4.png" alt="">
						<figcaption class="advantages_item_title"><span class="advantages_item_title_content">Постоянное наличие на складах дилеров, в регионах в том числе</span></figcaption>
					</figure>
				</div>
				<a href="#" class="main-btn">стать дилером</a>
			</div>
		</section>
		<section class="clients">
			<h2 class="main_title">нам доверяют</h2>
			<div class="container">
				<div class="row default_slider">
					<span class="default_slider_arr default_slider_arr_l client"></span>
					<div id="clients-slider" class="default_slider_content">
						<span class="default_slider_content_item"><img src="img/client-img1.jpg"></span>
						<span class="default_slider_content_item"><img src="img/client-img2.jpg"></span>
						<span class="default_slider_content_item"><img src="img/client-img3.jpg"></span>
						<span class="default_slider_content_item"><img src="img/client-img4.jpg"></span>
						<span class="default_slider_content_item"><img src="img/client-img5.jpg"></span>
					</div>
					<span class="default_slider_arr default_slider_arr_r client"></span>
				</div>
			</div>
		</section>
		<section class="production">
			<h2 class="main_title">наше производство</h2>
			<div class="container">
				<div class="row default_slider">
					<span class="default_slider_arr default_slider_arr_l prod"></span>
					<div id="prod-slider" class="default_slider_content">
						<a href="img/honor-img.jpg" class="default_slider_content_item" rel="production">
							<img src="img/prod-img1.png">
						</a>
						<a href="img/honor-img.jpg" class="default_slider_content_item" rel="production">
							<img src="img/prod-img2.png">
						</a>
						<a href="img/honor-img.jpg" class="default_slider_content_item" rel="production">
							<img src="img/prod-img3.png">
						</a>
						<a href="img/honor-img.jpg" class="default_slider_content_item" rel="production">
							<img src="img/prod-img4.png">
						</a>
						<a href="img/honor-img.jpg" class="default_slider_content_item" rel="production">
							<img src="img/prod-img1.png">
						</a>
					</div>
					<span class="default_slider_arr default_slider_arr_r prod"></span>
				</div>
			</div>
		</section>
		<section class="honors">
			<h2 class="main_title">награды и рекомендации</h2>
			<div class="container">
				<div class="row default_slider">
					<span class="default_slider_arr default_slider_arr_l honor"></span>
					<div id="honor-slider" class="default_slider_content">
						<a href="img/honor-img.jpg" class="default_slider_content_item" rel="honor">
							<img src="img/honor-img1.jpg">
						</a>
						<a href="img/honor-img.jpg" class="default_slider_content_item" rel="honor">
							<img src="img/honor-img1.jpg">
						</a>
						<a href="img/honor-img.jpg" class="default_slider_content_item" rel="honor">
							<img src="img/honor-img1.jpg">
						</a>
						<a href="img/honor-img.jpg" class="default_slider_content_item" rel="honor">
							<img src="img/honor-img1.jpg">
						</a>
						<a href="img/honor-img.jpg" class="default_slider_content_item" rel="honor">
							<img src="img/honor-img1.jpg">
						</a>
					</div>
					<span class="default_slider_arr default_slider_arr_r honor"></span>
				</div>
			</div>
		</section>
		<section class="map">
			<h2 class="main_title">наши дилеры</h2>
			<div id="map" style="width: 100%; height: 740px"></div>
		</section>
	</article>
	<?php
	include 'footer.php';
	?>
</div>
<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.fancybox.js?v=2.1.5" type="text/javascript" ></script>
<script src="js/script.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		$('a[rel=honor]').fancybox({
			padding		: 0,
			prevEffect : 'fade',
			nextEffect : 'fade',
			helpers : {
				overlay : {
					css : {
						'background' : 'rgba(0, 0, 0, 0.7)'
					}
				},
				title : {
					type : 'inside'
				},
				buttons	: {},
				preload   : true
			},
			afterLoad : function() {
				this.title = '<span class="fancybox-title-over">' + '<span class="fancybox-title-pagination">' + (this.index + 1) + ' of ' + this.group.length + '</span>' + '<span class="fancybox-title-text">' + (this.title ? '' + this.title : '') + '</span>' + '</span>';
			}
		});
	});
	$('a[rel=honor]').eq(0).trigger('click');
</script>
<script type="text/javascript">
	$(document).ready(function() {
		$('a[rel=production]').fancybox({
			padding		: 0,
			prevEffect : 'fade',
			nextEffect : 'fade',
			helpers : {
				overlay : {
					css : {
						'background' : 'rgba(0, 0, 0, 0.7)'
					}
				},
				title : {
					type : 'inside'
				},
				buttons	: {},
				preload   : true
			},
			afterLoad : function() {
				this.title = '<span class="fancybox-title-over">' + '<span class="fancybox-title-pagination">' + (this.index + 1) + ' of ' + this.group.length + '</span>' + '<span class="fancybox-title-text">' + (this.title ? '' + this.title : '') + '</span>' + '</span>';
			}
		});
	});
	$('a[rel=production]').eq(0).trigger('click');
</script>
</body>
</html>