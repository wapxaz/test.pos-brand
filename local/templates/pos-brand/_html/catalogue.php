<!DOCTYPE html>
<html lang="ru">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, height=device-height">
	<meta name="viewport" content="width=device-width">
	<meta charset="utf-8">
	<title>ПРОИЗВОДИТЕЛЬ POS-МАТЕРИАЛОВ</title>
	<link rel="shortcut icon" href="img/favicon.ico" type="image/vnd.microsoft.icon">
	<link href="css/bootstrap.min.css" rel="stylesheet" media="all">
	<link rel="stylesheet" href="css/owl.carousel.css">
	<link rel="stylesheet" href="css/owl.theme.css">
	<link href="css/jquery.fancybox.css" rel="stylesheet" media="all">
	<link href="css/style.css" rel="stylesheet" media="all">
</head>
<body>
<a href="#0" class="cd-top"></a>
<div class="wrapper">
	<?php
	include 'header.php';
	?>
	<article class="main">
		<div class="container">
			<section class="breadcrumbs">
				<a href="index.html" class="breadcrumb-prev">Главная</a>
				<a href="#" class="breadcrumb-current">Каталог</a>
			</section>
			<h1 class="main_title">каталог</h1>
			<section class="catalog">
				<div class="row fs">
						<figure class="catalog_item col-md-3 col-sm-3 col-xs-6">
							<a href="#" class="catalog_item_content">
								<span class="catalog_item_title"><span class="catalog_item_title_content">рамочные стенды fold-up</span></span>
								<img src="img/catalog-img1.jpg" alt="">
							</a>
						</figure>
						<figure class="catalog_item col-md-3 col-sm-3 col-xs-6">
							<a href="#" class="catalog_item_content">
								<span class="catalog_item_title"><span class="catalog_item_title_content">стойки для печатной продукции</span></span>
								<img src="img/catalog-img2.jpg" alt="">
							</a>
						</figure>
						<figure class="catalog_item col-md-3 col-sm-3 col-xs-6">
							<a href="#" class="catalog_item_content">
								<span class="catalog_item_title"><span class="catalog_item_title_content">промостойки, ресепшн</span></span>
								<img src="img/catalog-img3.jpg" alt="">
							</a>
						</figure>
						<figure class="catalog_item col-md-3 col-sm-3 col-xs-6">
							<a href="#" class="catalog_item_content">
								<span class="catalog_item_title"><span class="catalog_item_title_content">Визуальная навигация</span></span>
								<img src="img/catalog-img4.jpg" alt="">
							</a>
						</figure>
						<figure class="catalog_item col-md-3 col-sm-3 col-xs-6">
							<a href="#" class="catalog_item_content">
								<span class="catalog_item_title"><span class="catalog_item_title_content">многорамочные системы</span></span>
								<img src="img/catalog-img5.jpg" alt="">
							</a>
						</figure>
						<figure class="catalog_item col-md-3 col-sm-3 col-xs-6">
							<a href="#" class="catalog_item_content">
								<span class="catalog_item_title"><span class="catalog_item_title_content">рекламные аксессуары</span></span>
								<img src="img/catalog-img6.jpg" alt="">
							</a>
						</figure>
						<figure class="catalog_item col-md-3 col-sm-3 col-xs-6">
							<a href="#" class="catalog_item_content">
								<span class="catalog_item_title"><span class="catalog_item_title_content">карманы, подставки</span></span>
								<img src="img/catalog-img7.jpg" alt="">
							</a>
						</figure>
						<figure class="catalog_item col-md-3 col-sm-3 col-xs-6">
							<a href="#" class="catalog_item_content">
								<span class="catalog_item_title"><span class="catalog_item_title_content">стойки металлические</span></span>
								<img src="img/catalog-img8.jpg" alt="">
							</a>
						</figure>
					</div>
			</section>
		</div>
	</article>
	<?php
	include 'footer.php';
	?>
</div>
<script src="js/jquery-2.1.3.min.js" type="text/javascript"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/script.js"></script>
</body>
</html>