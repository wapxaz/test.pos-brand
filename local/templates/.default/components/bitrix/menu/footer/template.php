<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?
if (!empty($arResult)):
	$arRows = array_chunk($arResult, 2);
	foreach ($arRows as $arRow) {
		?><section class="footer_production col-md-3 col-sm-3 col-xs-6"><?
		foreach ($arRow as $arItem) {
			?><a href="<?=$arItem["LINK"]?>" class="footer_production-item"><?=$arItem["TEXT"]?></a><?
		}
		?></section><?
	}
endif;
?>