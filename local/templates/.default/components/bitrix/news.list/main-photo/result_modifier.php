<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

foreach ($arResult['ITEMS'] as &$arItem) {
	$arItem['PICTURE'] = array_change_key_case(CFile::ResizeImageGet($arItem['PREVIEW_PICTURE'], array('width'=>1600, 'height'=>1000), BX_RESIZE_IMAGE_PROPORTIONAL, true), CASE_UPPER);
	$arTmp = getimagesize($_SERVER['DOCUMENT_ROOT'].$arItem['PICTURE']['SRC']);
	$arItem['PICTURE']['WIDTH'] = $arTmp[0];
	$arItem['PICTURE']['HEIGHT'] = $arTmp[1];

	$arItem['PREVIEW_PICTURE'] = array_change_key_case(CFile::ResizeImageGet($arItem['PREVIEW_PICTURE'], array('width'=>284, 'height'=>284), BX_RESIZE_IMAGE_EXACT, true), CASE_UPPER);
	$arTmp = getimagesize($_SERVER['DOCUMENT_ROOT'].$arItem['PREVIEW_PICTURE']['SRC']);
	$arItem['PREVIEW_PICTURE']['WIDTH'] = $arTmp[0];
	$arItem['PREVIEW_PICTURE']['HEIGHT'] = $arTmp[1];
	$arItem['PREVIEW_PICTURE']['SRC'] = prepareRhombPic($arItem['PREVIEW_PICTURE']['SRC']);
}