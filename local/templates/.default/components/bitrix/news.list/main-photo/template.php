<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<section class="production">
	<h2 class="main_title"><?=(CNLSMainSettings::GetSiteSetting('pos_main_photo_title') ? CNLSMainSettings::GetSiteSetting('pos_main_photo_title') : 'наше производство')?></h2>
	<div class="container">
		<div class="row default_slider">
			<span class="default_slider_arr default_slider_arr_l prod"></span>
			<div id="prod-slider" class="default_slider_content">
				<?foreach($arResult["ITEMS"] as $arItem):?>
					<?
					$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
					$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
					?>
					<a href="<?=$arItem['PICTURE']['SRC']?>" class="default_slider_content_item" rel="production" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
						<img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>">
					</a>
				<?endforeach;?>
			</div>
			<span class="default_slider_arr default_slider_arr_r prod"></span>
		</div>
	</div>
</section>