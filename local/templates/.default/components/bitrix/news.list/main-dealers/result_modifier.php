<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$arResult['REGIONS'] = array();
foreach ($arResult['ITEMS'] as &$arItem) {
	$arItem['THUMB'] = array_change_key_case(CFile::ResizeImageGet($arItem['PREVIEW_PICTURE'], array('width'=>42, 'height'=>42), BX_RESIZE_IMAGE_PROPORTIONAL, true), CASE_UPPER);
	$arTmp = getimagesize($_SERVER['DOCUMENT_ROOT'].$arItem['THUMB']['SRC']);
	$arItem['THUMB']['WIDTH'] = $arTmp[0];
	$arItem['THUMB']['HEIGHT'] = $arTmp[1];

	$arItem['PREVIEW_PICTURE'] = array_change_key_case(CFile::ResizeImageGet($arItem['PREVIEW_PICTURE'], array('width'=>77, 'height'=>200), BX_RESIZE_IMAGE_PROPORTIONAL, true), CASE_UPPER);
	$arTmp = getimagesize($_SERVER['DOCUMENT_ROOT'].$arItem['PREVIEW_PICTURE']['SRC']);
	$arItem['PREVIEW_PICTURE']['WIDTH'] = $arTmp[0];
	$arItem['PREVIEW_PICTURE']['HEIGHT'] = $arTmp[1];

	foreach ($arItem['PROPERTIES']['REGION']['VALUE'] as $key => $value) {
		if(!isset($arResult['REGIONS'][ $arItem['PROPERTIES']['REGION']['VALUE_XML_ID'][$key] ])) {
			$arResult['REGIONS'][ $arItem['PROPERTIES']['REGION']['VALUE_XML_ID'][$key] ] = array(
				'XML_ID' => $arItem['PROPERTIES']['REGION']['VALUE_XML_ID'][$key],
				'NAME' => $value,
				'CNT' => 1,
			);
		} else {
			$arResult['REGIONS'][ $arItem['PROPERTIES']['REGION']['VALUE_XML_ID'][$key] ]['CNT']++;
		}
	}
}