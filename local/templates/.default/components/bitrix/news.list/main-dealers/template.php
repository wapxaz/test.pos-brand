<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<script>
window.bluePlacemarks = new Array;
</script>
<section class="map">
	<h2 class="main_title"><?=(CNLSMainSettings::GetSiteSetting('pos_main_dealers_title') ? CNLSMainSettings::GetSiteSetting('pos_main_dealers_title') : 'наши дилеры')?></h2>
	<div id="map" style="width: 100%; height: 740px"></div>
	<?
	$i = 0;
	foreach($arResult['ITEMS'] as $arItem):
		$arCoords = explode(',', $arItem['PROPERTIES']['COORDS']['~VALUE']);
	?>
	<script>
		window.bluePlacemarks[<?=$i?>] = new Object;
		window.bluePlacemarks[<?=$i?>]['id'] = '<?=$arItem['ID']?>';
		window.bluePlacemarks[<?=$i?>]['XML_ID'] = '<?=implode(',', $arItem['PROPERTIES']['REGION']['VALUE_XML_ID'])?>';
		window.bluePlacemarks[<?=$i?>]['iconContent'] = '<?=$arItem['NAME']?>';
		window.bluePlacemarks[<?=$i?>]['hintContent'] = '<?=$arItem['NAME']?>';
		window.bluePlacemarks[<?=$i?>]['coord_x'] = '<?=$arCoords[0]?>';
		window.bluePlacemarks[<?=$i?>]['coord_y'] = '<?=$arCoords[1]?>';
		window.bluePlacemarks[<?=$i?>]['balloonContentHeader'] = '<a href="http://<?=str_replace(array('http://', '//'), '', $arItem['PROPERTIES']['SITE']['VALUE'])?>" style=\"display: block; overflow: hidden;\"><span style=\"width: <?=$arItem["THUMB"]["WIDTH"]?>px; height: <?=$arItem["THUMB"]["HEIGHT"]?>px; display: inline-block; vertical-align: middle; \"><img src="<?=$arItem["THUMB"]["SRC"]?>" alt=""></span><span style=\"letter-spacing: 1px; font-size: 14px; font-family: \'PFDinDisplayLight\', Arial, sans-serif; font-weight: normal; color: #000; display: inline-block; vertical-align: middle; line-height: 16px; text-transform: uppercase; padding: 0 40px 0 5px; \"><?=$arItem['NAME']?></span></a>';
		window.bluePlacemarks[<?=$i?>]['balloonContentBody'] = "<div style=\"letter-spacing: 1px; line-height: 16px; font-size: 12px; font-weight: normal; font-family: \'PFDinDisplayLight\', Arial, sans-serif;\" ><a style=\"color: #4485a8;text-decoration:underline;\" target='_blank' href='http://<?=str_replace(array('http://', '//'), '', $arItem['PROPERTIES']['SITE']['VALUE'])?>' ><?=$arItem['PROPERTIES']['SITE']['VALUE']?></a><a style=\"color: #4485a8; display: block; border-bottom: 1px solid #aacbd9; padding-bottom: 7px;\" href='mailto:<?=$arItem['PROPERTIES']['EMAIL']['VALUE']?>'>e-mail: <?=$arItem['PROPERTIES']['EMAIL']['VALUE']?></a><p style=\"padding-top: 7px;\"><?=str_replace(array("\r", "\n"), '', $arItem['PROPERTIES']['ADDRESS']['~VALUE']['TEXT'])?></p><p>тел. <?=str_replace(array("\r", "\n"), '', $arItem['PROPERTIES']['PHONE']['~VALUE']['TEXT'])?></p></div>";
	</script>
	<?
		$i++;
	endforeach?>
</section>