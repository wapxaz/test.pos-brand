<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<section class="advantages">
	<h2 class="main_title"><?=(CNLSMainSettings::GetSiteSetting('pos_main_advantages_title') ? CNLSMainSettings::GetSiteSetting('pos_main_advantages_title') : 'наши преимущества')?></h2>
	<div class="container">
		<div class="advantages-info">индивидуальный подход при проектировании каждой отдельной рекламной системы <span>posbrand</span></div>
		<div class="row fs">
			<?foreach($arResult["ITEMS"] as $arItem):?>
				<?
				$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
				$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
				?>
				<figure class="advantages_item col-md-3 col-sm-3 col-xs-6" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
					<img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" alt="">
					<figcaption class="advantages_item_title"><span class="advantages_item_title_content"><?=$arItem['NAME']?></span></figcaption>
				</figure>
			<?endforeach;?>
		</div>
		<a href="/become-dealer/" class="main-btn">стать дилером</a>
	</div>
</section>