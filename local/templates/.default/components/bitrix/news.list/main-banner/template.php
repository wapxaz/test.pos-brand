<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<section id="slider" class="slider">
<?foreach($arResult["ITEMS"] as $k => $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
	<div class="slider-container <?if($k == 0):?>slider-container_visible<?endif?>" id="<?=$this->GetEditAreaId($arItem['ID']);?>" data-index="banner-index-<?=$arItem['ID']?>">
		<span class="slider_info-container <?if($k != 0):?>slider_info-container_hidden<?endif?>" data-index="banner-index-<?=$arItem['ID']?>">
			<span class="slider_info-container_text">
				<p class="slider-container_title"><?=$arItem['NAME']?></p>
				<p class="slider-container_text"><?=$arItem['PREVIEW_TEXT']?></p>
				<a href="<?=$arItem['PROPERTIES']['LINK']['VALUE']?>" class="main-btn"><?=($arItem['PROPERTIES']['LINK_NAME']['VALUE'] ? $arItem['PROPERTIES']['LINK_NAME']['VALUE'] : 'Подробнее')?></a>
			</span>
		</span>
		<?if($arItem['PREVIEW_PICTURE']['SRC']):?>
			<?if($k == 0):?>
				<img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" alt="" />
			<?else:?>
				<img src="<?=SITE_TEMPLATE_PATH?>/_html/img/blank.gif" class="preload-banner-image" data-index="banner-index-<?=$arItem['ID']?>" data-src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" alt="" />
			<?endif?>
		<?endif?>
	</div>
<?endforeach;?>
</section>