<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$strSectionEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_EDIT");
$strSectionDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_DELETE");
$arSectionDeleteParams = array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM'));

if (0 < $arResult["SECTIONS_COUNT"]):?>
	<section class="catalog">
		<div class="row fs">
<?foreach ($arResult['SECTIONS'] as &$arSection)
{
	$this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
	$this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);
	?>
			<figure class="catalog_item <?if($APPLICATION->GetCurDir() == "/catalog/"){?> col-md-3 col-sm-3<?}else{?> col-md-4 col-sm-4 <?}?> col-xs-6" id="<? echo $this->GetEditAreaId($arSection['ID']); ?>">
		<a href="<?=$arSection['SECTION_PAGE_URL']?>" class="catalog_item_content">
			<span class="catalog_item_title"><span class="catalog_item_title_content"><?=$arSection['NAME']?></span></span>
			<?if($arSection['PICTURE']['SRC']):?>
			<img src="<?=$arSection['PICTURE']['SRC']?>" alt="">
			<?else:?>
			<img src="<?=$arResult['EMPTY_PIC']?>" alt="">
			<?endif?>
		</a>
	</figure>
	<?
}
?>
		</div>
	</section>
<?endif?>