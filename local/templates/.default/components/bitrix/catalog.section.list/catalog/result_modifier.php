<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

foreach ($arResult['SECTIONS'] as &$arItem) {
	$arItem['PICTURE'] = array_change_key_case(CFile::ResizeImageGet($arItem['PICTURE'], array('width'=>260, 'height'=>227), BX_RESIZE_IMAGE_EXACT, true), CASE_UPPER);
	$arTmp = getimagesize($_SERVER['DOCUMENT_ROOT'].$arItem['PICTURE']['SRC']);
	$arItem['PICTURE']['WIDTH'] = $arTmp[0];
	$arItem['PICTURE']['HEIGHT'] = $arTmp[1];
}
if(CFile::ResizeImageFile(
	$_SERVER["DOCUMENT_ROOT"].CNLSMainSettings::GetSiteSetting('pos_tpl_empty_pic'),
	$destinationFile =  $_SERVER["DOCUMENT_ROOT"]."/upload/catalog_section_list_empty_pic.png",
	array('width'=>260, 'height'=>227),
	BX_RESIZE_IMAGE_EXACT
)) {
	$arResult['EMPTY_PIC'] = str_replace($_SERVER["DOCUMENT_ROOT"], '', $destinationFile);
}