<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>
<div class="js-ajax-form-container" data-href="/ajax/form.php">
	<?
	if(!strlen($arResult["FORM_NOTE"])) {
		echo $arResult["FORM_HEADER"];
		foreach ($arResult["QUESTIONS"] as $FIELD_SID => $arQuestion)
		{
			if ($arQuestion['STRUCTURE'][0]['FIELD_TYPE'] == 'hidden')
			{
				echo $arQuestion["HTML_CODE"];
			}
			else
			{
				?>
					<div class="form_item">
					<?/*if (is_array($arResult["FORM_ERRORS"]) && array_key_exists($FIELD_SID, $arResult['FORM_ERRORS'])):?>
					<span class="error-fld" title="<?=$arResult["FORM_ERRORS"][$FIELD_SID]?>"></span>
					<?endif;*/?>
						<label><?=$arQuestion["CAPTION"]?><?if ($arQuestion["REQUIRED"] == "Y"):?><span>*</span><?endif;?></label>
						<span class="form_item_input-container">
						<?
							switch ($arQuestion['STRUCTURE'][0]['FIELD_TYPE']) {
								case 'text':
									?>
									<input type="text" name="form_text_<?=$arQuestion['STRUCTURE'][0]['ID']?>" value="<?=$_POST['form_text_'.$arQuestion['STRUCTURE'][0]['ID']]?>" <?if ($arQuestion["REQUIRED"] == "Y"):?>required=""<?endif?>/>
									<?
									break;
								case 'date':
									?>
									<input type="text" class="js-date-input" name="form_date_<?=$arQuestion['STRUCTURE'][0]['ID']?>" value="<?=$_POST['form_date_'.$arQuestion['STRUCTURE'][0]['ID']]?>" <?if ($arQuestion["REQUIRED"] == "Y"):?>required=""<?endif?>/>
									<?
									break;
								case 'textarea':
									?>
									<textarea name="form_textarea_<?=$arQuestion['STRUCTURE'][0]['ID']?>" <?if ($arQuestion["REQUIRED"] == "Y"):?>required="required"<?endif?>><?=$_POST['form_textarea_'.$arQuestion['STRUCTURE'][0]['ID']]?></textarea>
									<?
									break;
								case 'email':
									?>
									<input type="email" name="form_email_<?=$arQuestion['STRUCTURE'][0]['ID']?>" value="<?=$_POST['form_email_'.$arQuestion['STRUCTURE'][0]['ID']]?>" <?if ($arQuestion["REQUIRED"] == "Y"):?>required=""<?endif?>/>
									<?
									break;
								case 'dropdown':
									?>
										<select name="form_dropdown_<?=$FIELD_SID?>" <?if ($arQuestion["REQUIRED"] == "Y"):?>required="required"<?endif?>>
											<?foreach($arQuestion['STRUCTURE'] as $i => $arVal):?>
												<option value="<?=$arVal['ID']?>" <?if($arVal['ID'] == $_POST['form_dropdown_'.$FIELD_SID]) echo 'selected';?> ><?=(GetMessage("FORM_DROPDOWN_" . $FIELD_SID.'_'.$arVal['ID']) ? GetMessage("FORM_DROPDOWN_" . $FIELD_SID.'_'.$arVal['ID']) : $arVal['MESSAGE'])?></option>
											<?endforeach?>
										</select>
									<?
									break;
								
								default:
									echo $arQuestion["HTML_CODE"];
									break;
							}
						?>
						</span>
					</div>
				<?
			}
		}
		if($arResult["isUseCaptcha"] == "Y")
		{
			?>
				<div class="form_item">
					<label>Введите код с картинки<span>*</span></label>
					<span class="form_item_input-container">
						<input type="hidden" name="captcha_sid" value="<?=htmlspecialcharsbx($arResult["CAPTCHACode"]);?>" /><img src="/bitrix/tools/captcha.php?captcha_sid=<?=htmlspecialcharsbx($arResult["CAPTCHACode"]);?>" width="180" height="40" />
						<input type="text" name="captcha_word" size="30" maxlength="50" value="" />
					</span>
				</div>
			<?
		}
		?>
		<input type="hidden" name="web_form_apply" value="Y" />
		<div class="form_item_btn_container">
			<input class="form_item_btn" type="submit" name="web_form_apply" value="Отправить">
		</div>
		<p class="form_item_p"><span class="form_item_p_imp">*</span> - все поля обязательны для заполнения</p>
		<?if ($arResult["isFormErrors"] == "Y"):?><?=$arResult["FORM_ERRORS_TEXT"];?><?endif;?>
		<?=$arResult["FORM_FOOTER"]?>
	<?}?>
	<div class="form_item">
		<?=$arResult["FORM_NOTE"]?>
	</div>
</div>