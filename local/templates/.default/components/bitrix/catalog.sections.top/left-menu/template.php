<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div id="sidebar-catalog-menu" class="sidebar_title">
	<span>каталог</span>
	<button type="button" class="navbar-toggle">
		<span class="sr-only">Toggle navigation</span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
	</button>
</div>
<ul id="sidebar-catalog-menu-list" class="sidebar_menu-second">
	<?foreach($arResult["SECTIONS"] as $arSection):?>
		<li><a href="<?=$arSection["SECTION_PAGE_URL"]?>" class="<?if(strpos($APPLICATION->GetCurDir(), $arSection["SECTION_PAGE_URL"]) !== false) echo 'sidebar_menu-second_link_drop-main'; else echo 'sidebar_menu-second_link';?>"><?=$arSection["NAME"]?></a></li>
		<?if(strpos($APPLICATION->GetCurDir(), $arSection["SECTION_PAGE_URL"]) !== false):?>
			<ul class="sidebar_menu-second_link sidebar_menu-second_link_drop">
				<?foreach($arSection["ITEMS"] as $arElement):?>
				<?
				$this->AddEditAction($arElement['ID'], $arElement['EDIT_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT"));
				$this->AddDeleteAction($arElement['ID'], $arElement['DELETE_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BCST_ELEMENT_DELETE_CONFIRM')));
				?>
					<li><a href="<?=$arElement['DETAIL_PAGE_URL']?>" class="sidebar_menu-second_link-inner <?if($APPLICATION->GetCurDir() == $arElement['DETAIL_PAGE_URL']) echo 'sidebar_menu-second_link-inner_active';?>"><?=$arElement['NAME']?></a></li>
				<?endforeach?>
			</ul>
		<?endif?>
	<?endforeach?>
</ul>