<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<section class="product">
	<div class="product_images">
		<?if(is_array($arResult['PROPERTIES']['COLOR']['PROPERTY_VALUE_ID']) && count($arResult['PROPERTIES']['COLOR']['PROPERTY_VALUE_ID'])):?>
			<?foreach($arResult['PROPERTIES']['COLOR']['PROPERTY_VALUE_ID'] as $k => $colorID):?>
				<div class="images-slider <?if($k !== 0) echo 'hidden-block';?>" id="images-slider-<?=$colorID?>" data-thumbnail-id="#images-slider_thumbnail-<?=$colorID?>" data-color-id="<?=$colorID?>">
					<?if(count($arResult['COLOR_PHOTO'][ $colorID ]) > 0):?>
						<?foreach($arResult['COLOR_PHOTO'][ $colorID ] as $arPhoto):?>
						<a href="<?=$arPhoto['BIG']['SRC']?>" class="product_images_img" rel="image_<?=$colorID?>">
							<img src="<?=$arPhoto['MEDIUM']['SRC']?>" alt="">
						</a>
						<?endforeach?>
					<?else:?>
						<span class="product_images_img">
							<img src="<?=$arResult['EMPTY_PIC']?>" alt="">
						</span>
					<?endif?>
				</div>
				<div class="images-thumbnail <?if($k !== 0) echo 'hidden-block';?>" data-color-id="<?=$colorID?>">
					<?if(count($arResult['COLOR_PHOTO'][ $colorID ]) > 1):?>
						<span class="images-slider_arr images-slider_arr_l"></span>
						<span class="images-slider_arr images-slider_arr_r"></span>
						<div class="images-slider_thumbnail" id="images-slider_thumbnail-<?=$colorID?>">
							<?foreach($arResult['COLOR_PHOTO'][ $colorID ] as $arPhoto):?>
							<a href="#" class="product_images_thumbnail">
								<img src="<?=$arPhoto['THUMB']['SRC']?>" alt="">
							</a>
							<?endforeach?>
						</div>
					<?endif?>
				</div>
			<?endforeach?>
		<?else:?>
			<div class="images-slider">
				<span class="product_images_img">
					<img src="<?=$arResult['EMPTY_PIC']?>" alt="">
				</span>
			</div>
			<div class="images-thumbnail">
			</div>
		<?endif?>
	</div>
	<h1 class="main_title"><?=$arResult['NAME']?></h1>
	<div class="product_descr"><?=$arResult['PREVIEW_TEXT']?></div>
	<?if(is_array($arResult['PROPERTIES']['COLOR']['PROPERTY_VALUE_ID']) && count($arResult['PROPERTIES']['COLOR']['PROPERTY_VALUE_ID'])):?>
		<div class="product_colors">
			<div class="product_block_title">цвета</div>
			<?foreach($arResult['PROPERTIES']['COLOR']['~VALUE'] as $k => $strColor):?>
			<span class="product_colors_item js-change-color <?if($k == 0) echo 'product_colors_item_active';?>" data-color-id="<?=$arResult['PROPERTIES']['COLOR']['PROPERTY_VALUE_ID'][$k]?>">
				<span class="product_colors_item_color" style="background: <?=$strColor?>"></span>
				<span class="product_colors_item_title"><?=$arResult['PROPERTIES']['COLOR']['DESCRIPTION'][$k]?></span>
			</span>
			<?endforeach?>
		</div>
	<?endif?>
	<div class="product_parameters">
		<table class="product_parameters_content">
			<tr class="product_block_title_container">
				<?foreach($arResult['PROPERTIES']['COLUMNS']['~VALUE'] as $strName):?>
				<th class="product_block_title"><?=$strName?></th>
				<?endforeach?>
			</tr>
			<?foreach($arResult['PROPERTIES']['MODIFICATIONS']['~VALUE'] as $arValue):?>
				<tr class="product_parameters_item">
					<?foreach($arResult['PROPERTIES']['COLUMNS']['PROPERTY_VALUE_ID'] as $nValID):?>
						<td class="product_parameters_item_content"><?=$arValue[$nValID]?></td>
					<?endforeach?>
				</tr>
			<?endforeach?>
		</table>
	</div>
	<?
		if(!is_array($arResult['PROPERTIES']['TAB_ORDER']['VALUE']) or !count($arResult['PROPERTIES']['TAB_ORDER']['VALUE'])) {
			$arResult['PROPERTIES']['TAB_ORDER']['VALUE'] = array(
				'DETAIL_TEXT',
				'CHARS',
				'CERTS',
				'VIDEO',
				'INSTRUCTIONS',
				'ADDS',
			);
		}
	?>
	<div class="product_tabs">
		<div class="product_block_title_container product_block_tabs">
			<?foreach($arResult['PROPERTIES']['TAB_ORDER']['VALUE'] as $strTabID) {
				$strTabID = strtoupper($strTabID);
				switch ($strTabID) {
					case 'DETAIL_TEXT':
						if(strlen($arResult['DETAIL_TEXT'])) { ?>
						<span class="product_block_title active">описание</span>
						<? }
						break;
					case 'CHARS':
						if(is_array($arResult['PROPERTIES']['CHARS']['~VALUE']) && count($arResult['PROPERTIES']['CHARS']['~VALUE'])) { ?>
						<span class="product_block_title">характеристики</span>
						<? }
						break;
					case 'CERTS':
						if(is_array($arResult['CERTS']) && count($arResult['CERTS'])) { ?>
						<span class="product_block_title">сертификаты</span>
						<? }
						break;
					case 'VIDEO':
						if(is_array($arResult['PROPERTIES']['VIDEO']['~VALUE'])) { ?>
						<span class="product_block_title">видео</span>
						<? }
						break;
					case 'INSTRUCTIONS':
						if(is_array($arResult['INSTRUCTIONS']) && count($arResult['INSTRUCTIONS'])) { ?>
						<span class="product_block_title">инструкция по сборке</span>
						<? }
						break;
					case 'ADDS':
						foreach ($arResult['PROPERTIES']['TAB_CONTENT']['DESCRIPTION'] as $strTitle) {
							?><span class="product_block_title"><?=$strTitle?></span><?
						}
						break;
					
					default:
						# code...
						break;
				}
			}
			?>
		</div>
		<?foreach($arResult['PROPERTIES']['TAB_ORDER']['VALUE'] as $strTabID) {
			$strTabID = strtoupper($strTabID);
			switch ($strTabID) {
				case 'DETAIL_TEXT':
					if(strlen($arResult['DETAIL_TEXT'])) { ?>
					<div class="product_tabs_content active">
						<?=$arResult['DETAIL_TEXT']?>
					</div>
					<?
					}
					break;
				case 'CHARS':
					if(is_array($arResult['PROPERTIES']['CHARS']['~VALUE']) && count($arResult['PROPERTIES']['CHARS']['~VALUE'])) { ?>
					<div class="product_tabs_content">
						<h3 class="third-title">ОСНОВНЫЕ ХАРАКТЕРИСТИКИ</h3>
						<?foreach($arResult['PROPERTIES']['CHARS']['~VALUE'] as $k => $strChar):?>
						<div class="product_tabs_content_item">
							<span class="product_tabs_content_item_title"><?=$strChar?>:</span>
							<span class="product_tabs_content_item_content"><?=$arResult['PROPERTIES']['CHARS']['DESCRIPTION'][$k]?></span>
						</div>
						<?endforeach?>
					</div>
					<?
					}
					break;
				case 'CERTS':
					if(is_array($arResult['CERTS']) && count($arResult['CERTS'])) { ?>
					<div class="product_tabs_content">
						<div class="product_certificates row">
							<?foreach($arResult['CERTS'] as $arPhoto):?>
							<a href="<?=$arPhoto['BIG']['SRC']?>" class="col-md-3 col-sm-4 col-xs-3 product_certificates_item" rel="certificates">
								<span class="img-zoom">
									<img src="<?=$arPhoto['THUMB']['SRC']?>">
								</span>
							</a>
							<?endforeach?>
						</div>
					</div>
					<?
					}
					break;
				case 'VIDEO':
					if(is_array($arResult['PROPERTIES']['VIDEO']['~VALUE'])) { ?>
					<div class="product_tabs_content">
						<?foreach($arResult['PROPERTIES']['VIDEO']['~VALUE'] as $k => $strVideoLink):?>
						<h3 class="third-title"><?=$arResult['PROPERTIES']['VIDEO']['DESCRIPTION'][$k]?></h3>
						<iframe width="490" height="275" src="<?=$strVideoLink?>" frameborder="0" allowfullscreen=""></iframe>
						<?endforeach?>
					</div>
					<?
					}
					break;
				case 'INSTRUCTIONS':
					if(is_array($arResult['INSTRUCTIONS']) && count($arResult['INSTRUCTIONS'])) { ?>
					<div class="product_tabs_content">
						<h3 class="third-title">ИНСТРУКЦИИ ПО СБОРКЕ РАЗЛИЧНЫХ МОДЕЛЕЙ</h3>
						<div class="fs">
							<?foreach($arResult['INSTRUCTIONS'] as $arFile):?>
							<div class="col-md-6 col-sm-6 col-xs-6 product_instruction <?=strtolower($arFile['EXT'])?>">
								<span class="product_instruction_doc"></span>
								<span class="product_instruction_title"><?=$arFile['NAME']?></span>
								<span class="product_instruction_format"><?=strtoupper($arFile['EXT'])?> (<?=$arFile['FILESIZE']?>)</span>
								<a href="<?=$arFile['SRC']?>" class="product_instruction_download">СКАЧАТЬ</a>
							</div>
							<?endforeach?>
						</div>
					</div>
					<?
					}
					break;
				case 'ADDS':
					foreach ($arResult['PROPERTIES']['TAB_CONTENT']['~VALUE'] as $arText) {
						?>
						<div class="product_tabs_content">
							<?=$arText['TEXT']?>
						</div><?
					}
					break;
				
				default:
					# code...
					break;
			}
		}
		?>
	</div>
	<div class="product_questions">
		<div class="product_questions_title">ОСТАЛИСЬ ВОПРОСЫ?
			<span>ЗАДАЙТЕ ИХ НАШИМ СПЕЦИАЛИСТАМ!</span>
		</div>
		<div class="product_questions_phone">
			<?if(CNLSMainSettings::GetSiteSetting('pos_item_card_phone')):?>
			<?=CNLSMainSettings::GetSiteSetting('pos_item_card_phone')?> <br>
			<?endif?>
			<a href="#order-call" onclick="openPopup(this);">ЗАКАЗАТЬ ОБРАТНЫЙ ЗВОНОК</a>
		</div>
	</div>
</section>
<script type="text/javascript">
	$(document).ready(function() {
		$('a.product_images_img').fancybox({
			padding		: 0,
			prevEffect : 'fade',
			nextEffect : 'fade',
			helpers : {
				overlay : {
					css : {
						'background' : 'rgba(0, 0, 0, 0.7)'
					}
				},
				title : {
					type : 'inside'
				},
				buttons	: {},
				preload   : true
			},
			afterLoad : function() {
				this.title = '<span class="fancybox-title-over">' + '<span class="fancybox-title-pagination">' + (this.index + 1) + ' of ' + this.group.length + '</span>' + '<span class="fancybox-title-text">' + (this.title ? '' + this.title : '') + '</span>' + '</span>';
			}
		});
	});
	$('a.product_images_img').eq(0).trigger('click');
</script>