<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

foreach ($arResult['PROPERTIES']['COLOR_PHOTO']['VALUE'] as $nColorID => $arPhotos) {
	foreach ($arPhotos as $nFile) {
		$arPhoto = array();
		$arPhoto['BIG'] = array_change_key_case(CFile::ResizeImageGet($nFile, array('width'=>1600, 'height'=>1000), BX_RESIZE_IMAGE_PROPORTIONAL, true), CASE_UPPER);
		$arTmp = getimagesize($_SERVER['DOCUMENT_ROOT'].$arPhoto['BIG']['SRC']);
		$arPhoto['BIG']['WIDTH'] = $arTmp[0];
		$arPhoto['BIG']['HEIGHT'] = $arTmp[1];

		$arPhoto['MEDIUM'] = array_change_key_case(CFile::ResizeImageGet($nFile, array('width'=>331, 'height'=>376), BX_RESIZE_IMAGE_PROPORTIONAL, true), CASE_UPPER);
		$arTmp = getimagesize($_SERVER['DOCUMENT_ROOT'].$arPhoto['MEDIUM']['SRC']);
		$arPhoto['MEDIUM']['WIDTH'] = $arTmp[0];
		$arPhoto['MEDIUM']['HEIGHT'] = $arTmp[1];

		$arPhoto['THUMB'] = array_change_key_case(CFile::ResizeImageGet($nFile, array('width'=>50, 'height'=>48), BX_RESIZE_IMAGE_PROPORTIONAL, true), CASE_UPPER);
		$arTmp = getimagesize($_SERVER['DOCUMENT_ROOT'].$arPhoto['THUMB']['SRC']);
		$arPhoto['THUMB']['WIDTH'] = $arTmp[0];
		$arPhoto['THUMB']['HEIGHT'] = $arTmp[1];

		$arResult['COLOR_PHOTO'][$nColorID][] = $arPhoto;
	}
}

$i = 0;
foreach ($arResult['PROPERTIES']['CERTS']['VALUE'] as $arPic) {
	$arResult['CERTS'][$i]['BIG'] = array_change_key_case(CFile::ResizeImageGet($arPic, array('width'=>1600, 'height'=>1000), BX_RESIZE_IMAGE_PROPORTIONAL, true), CASE_UPPER);
	$arTmp = getimagesize($_SERVER['DOCUMENT_ROOT'].$arResult['CERTS'][$i]['BIG']['SRC']);
	$arResult['CERTS'][$i]['BIG']['WIDTH'] = $arTmp[0];
	$arResult['CERTS'][$i]['BIG']['HEIGHT'] = $arTmp[1];

	$arResult['CERTS'][$i]['THUMB'] = array_change_key_case(CFile::ResizeImageGet($arPic, array('width'=>189, 'height'=>263), BX_RESIZE_IMAGE_EXACT, true), CASE_UPPER);
	$arTmp = getimagesize($_SERVER['DOCUMENT_ROOT'].$arResult['CERTS'][$i]['THUMB']['SRC']);
	$arResult['CERTS'][$i]['THUMB']['WIDTH'] = $arTmp[0];
	$arResult['CERTS'][$i]['THUMB']['HEIGHT'] = $arTmp[1];
	$i++;
}

foreach ($arResult['PROPERTIES']['INSTRUCTIONS']['VALUE'] as $k => $nFile) {
	$strSrc = CFile::GetPath($nFile);
	$strExt = GetFileExtension($strSrc);
	$strName = explode('/', $strSrc);
	$strName = str_replace('.'.$strExt, '', $strName[ count($strName) - 1 ]);
	$arFile = array(
		'SRC' => $strSrc,
		'NAME' => ($arResult['PROPERTIES']['INSTRUCTIONS']['DESCRIPTION'][$k] ? $arResult['PROPERTIES']['INSTRUCTIONS']['DESCRIPTION'][$k] : $strName),
		'EXT' => $strExt,
		'FILESIZE' => CFile::FormatSize(filesize($_SERVER['DOCUMENT_ROOT'].$strSrc), 5)
	);
	$arResult['INSTRUCTIONS'][] = $arFile;
}
if(CFile::ResizeImageFile(
	$_SERVER["DOCUMENT_ROOT"].CNLSMainSettings::GetSiteSetting('pos_tpl_empty_pic'),
	$destinationFile =  $_SERVER["DOCUMENT_ROOT"]."/upload/catalog_element_empty_pic.png",
	array('width'=>331, 'height'=>376),
	BX_RESIZE_IMAGE_EXACT
)) {
	$arResult['EMPTY_PIC'] = str_replace($_SERVER["DOCUMENT_ROOT"], '', $destinationFile);
}