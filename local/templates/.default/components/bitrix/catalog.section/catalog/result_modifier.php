<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

foreach ($arResult['ITEMS'] as &$arItem) {
	$arItem['PREVIEW_PICTURE'] = array_change_key_case(CFile::ResizeImageGet($arItem['PREVIEW_PICTURE'], array('width'=>260, 'height'=>227), BX_RESIZE_IMAGE_PROPORTIONAL, true), CASE_UPPER);
	$arTmp = getimagesize($_SERVER['DOCUMENT_ROOT'].$arItem['PREVIEW_PICTURE']['SRC']);
	$arItem['PREVIEW_PICTURE']['WIDTH'] = $arTmp[0];
	$arItem['PREVIEW_PICTURE']['HEIGHT'] = $arTmp[1];
}
if(CFile::ResizeImageFile(
	$_SERVER["DOCUMENT_ROOT"].CNLSMainSettings::GetSiteSetting('pos_tpl_empty_pic'),
	$destinationFile =  $_SERVER["DOCUMENT_ROOT"]."/upload/catalog_section_empty_pic.png",
	array('width'=>260, 'height'=>227),
	BX_RESIZE_IMAGE_PROPORTIONAL
)) {
	$arResult['EMPTY_PIC'] = str_replace($_SERVER["DOCUMENT_ROOT"], '', $destinationFile);
}