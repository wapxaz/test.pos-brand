<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<section class="catalog">
	<div class="row fs">
<?
if (!empty($arResult['ITEMS']))
{
	foreach ($arResult['ITEMS'] as $key => $arItem)
	{
		$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], $strElementEdit);
		$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], $strElementDelete, $arElementDeleteParams);
	?>
		<figure class="catalog_item col-md-4 col-sm-4 col-xs-8" id="<? echo $this->GetEditAreaId($arItem['ID']); ?>">
			<a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="catalog_item_content">
				<span class="catalog_item_title"><span class="catalog_item_title_content"><?=$arItem['NAME']?></span></span>
				<?if($arItem['PREVIEW_PICTURE']['SRC']):?>
				<div class="catalog-img"><img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" alt=""></div>
				<?else:?>
				<div class="catalog-img"><img src="<?=$arResult['EMPTY_PIC']?>" alt=""></div>
				<?endif?>
			</a>
		</figure>
	<?
	}
}
?>
	</div>
	<?=$arResult["NAV_STRING"];?>
</section>